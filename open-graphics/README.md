General graphics to be shared
=============================

Chapter logos and some other book-related graphics that appear on the website are stored separately in subfolders of  `../public`
Metabolic map of E. coli central metabolism
===========================================

A diagram showing the reactions, metabolites, co-factors, and enzymes in the central metabolism of Escherichia coli bacteria. A few selected carbon sources and their catabolic pathways are shown as well. The original graphics (in svg format) can be found on zenodo (https://zenodo.org/records/11472550).
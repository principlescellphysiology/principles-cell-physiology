Files for Forum and book project
=============================================================

* `public`: HTML web pages
* `epcb-materials`: Different types of files for internal usage (advertisement slides, jingle, QR codes)
* `jupyter-notebooks`: Jupyter notebooks (planned to be exercises related to the book)
* `open-code`: General software code to share with others
* `open-graphics`: General graphics files to share with others
* `summer-school-materials`: Files related to the different summer schools

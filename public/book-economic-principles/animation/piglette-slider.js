let currentIndex = 0;
let isTransitioning = false;

function moveSlide(direction) {
  const slides = document.querySelectorAll('.slider-piglette');
  const totalSlides = slides.length;

  if (totalSlides === 0 || isTransitioning) return;

  isTransitioning = true;

  currentIndex += direction;
  if (currentIndex < 0) {
    currentIndex = 0;
  } else if (currentIndex >= totalSlides) {
    currentIndex = totalSlides - 1;
  }

  const slider = document.querySelector('.slider');
  slider.style.transition = 'transform 0.3s ease-in-out';
  slider.style.transform = `translateX(-${currentIndex * 100}%)`;

  updateButtonVisibility(slides);

  slider.addEventListener('transitionend', () => {
    isTransitioning = false;
  }, { once: true });
}

function updateButtonVisibility(slides) {
  const leftButton = document.querySelector('.slider-button.left');
  const rightButton = document.querySelector('.slider-button.right');
  const totalSlides = slides.length;

  leftButton.style.display = currentIndex === 0 ? 'none' : 'block';
  rightButton.style.display = currentIndex === totalSlides - 1 ? 'none' : 'block';
}

document.addEventListener('DOMContentLoaded', () => {
  const slides = document.querySelectorAll('.slider-piglette');
  updateButtonVisibility(slides);

  const leftButton = document.querySelector('.slider-button.left');
  const rightButton = document.querySelector('.slider-button.right');

  leftButton.addEventListener('click', () => moveSlide(-1));
  rightButton.addEventListener('click', () => moveSlide(1));
});

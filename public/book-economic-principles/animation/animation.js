(function() {
    'use strict';

    document.addEventListener("DOMContentLoaded", function() {
        const createSprite = () => {
            const sprite = document.createElement('div');
            sprite.id = 'sprite';
            const spriteStyles = {
                position: 'absolute',
                top: '200px',
                left: '-256px',
                width: '256px',
                height: '256px',
                backgroundImage: 'url(animation/animation.svg)',
                animation: 'sprite 0.6s steps(6) infinite, move 12s linear forwards',
                zIndex: '1',
                backgroundRepeat: 'no-repeat'
            };
            Object.assign(sprite.style, spriteStyles);
            const spriteContainer = document.getElementById('sprite-container');
            if (!spriteContainer) {
                const container = document.createElement('div');
                container.id = 'sprite-container';
                document.body.appendChild(container);
                container.appendChild(sprite);
            } else {
                spriteContainer.appendChild(sprite);
            }
            return sprite;
        };

        const sprite = createSprite();
        let removeSpriteTimeout;

        const removeSprite = () => {
            if (sprite && sprite.parentNode) {
                sprite.parentNode.removeChild(sprite);
            }
        };

        const clearRemoveSpriteTimeout = () => {
            if (removeSpriteTimeout) {
                clearTimeout(removeSpriteTimeout);
            }
        };

        removeSpriteTimeout = setTimeout(removeSprite, 12000);

        const clearAllTimeouts = () => {
            clearRemoveSpriteTimeout();
        };

        window.addEventListener('unload', clearAllTimeouts);
    });
})();

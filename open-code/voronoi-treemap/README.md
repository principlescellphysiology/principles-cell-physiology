# Voronoi Treemap

A project to help users that are not fluent in JavaScript to use the `D3` package for generating Voronoi Treemaps

## Dependencies

* `pip install` these packages:
    - colorutils
    - numpy
    - pandas

## Usage

* Clone the repository and install dependencies
* Edit the file `hierarchical_data.csv` to include your data:
    - The `level_1` column should contain the major groups (their names will appear as a legend)
    - The `level_2` column should contain the minor groups (which appear as text inside the tiles)
    - The `amount` column should represent the fraction (out of a total of 1)
    - The `hue` column should contain numbers between 0 and 255. Ideally, they should be uniform for each major group, and quite distinct (so that the legend would make sense).
* Run the command `python -m table_to_json`
* Run the command: `python -m http.server 8080 --bind 127.0.0.1`
* Open the following link in your browser [http://127.0.0.1:8080/](http://127.0.0.1:8080/)
* You should be able to see the result, you can try refreshing the page (F5) until you are satisfied with the arrangement of the tiles.
* Save the page by pressing ctrl+S (e.g. as `voronoi.html`)
* Open the `.html` file in a text editor and copy the `<svg ... </svg>` block to a separate file (e.g. `voronoi.svg`)
* Open the `.svg` file with Inkscape. You will see that all the tiles are black, but the actual colors are hidden behind black tiles. Using the ungroup function and some double-clicking, try to remove all the black tiles and save the result.

Example files for all the steps above can be found in the Git repository.
This is the result:
![Voronoi Treemap SVG](./voronoi.svg)


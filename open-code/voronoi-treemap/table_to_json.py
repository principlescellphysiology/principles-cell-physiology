import pandas as pd
import json
import colorutils
import numpy as np
import sys

hierarchical_df = pd.read_csv("hierarchical_data.csv")

root = dict()
root["name"] = "All Data"
root["children"] = []

for gr, group_df in hierarchical_df.groupby("level_1"):
    subtree = dict()
    subtree["name"] = gr
    subtree["color"] = colorutils.hsv_to_hex((group_df.hue.mean(), 0.5, 0.8))
    subtree["children"] = []

    for row in group_df.itertuples():
        v = 0.6 + 0.4 * np.random.rand()
        leaf = dict()
        leaf["name"] = row.level_2
        leaf["weight"] = row.amount
        leaf["color"] = colorutils.hsv_to_hex((row.hue, 0.5, v)).upper()
        subtree["children"].append(leaf)

    root["children"].append(subtree)

with open("hierarchical_data.json", "wt") as fp:
    json.dump(root, fp, indent="\t")

sys.stderr.write("""In order to see the Voronoi treemap:
* Run the command: `python -m http.server`
* Open the following link in your browser: http://0.0.0.0:8000/
""")

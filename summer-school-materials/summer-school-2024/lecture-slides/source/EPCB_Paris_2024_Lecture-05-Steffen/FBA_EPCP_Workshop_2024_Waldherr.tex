\documentclass[aspectratio=169]{beamer}

\usepackage{ifluatex}
%
\title{Optimization of metabolic fluxes}
\author{Steffen Waldherr}
\date{8th July 2024}

\newcommand{\species}[1]{{\mathrm{#1}}}
\newcommand{\T}{^\mathrm{T}}
\newcommand{\Real}{\mathbb{R}}


%
\definecolor{piggyred}{RGB}{255,212,213}
\definecolor{EPCPpurple}{RGB}{105,88,153}
\definecolor{EPCPpurpleMedium}{RGB}{159,139,213}
\definecolor{EPCPpurpleDark}{RGB}{31,0,81}
\definecolor{EPCPocher}{RGB}{254,244,254}
\definecolor{INRAEgreen}{RGB}{0,163,166}

\setbeamercolor{EPCP}{fg=white,bg=EPCPpurpleMedium}
\usecolortheme[named=EPCPpurple]{structure}
%
\setbeamerfont{title}{size=\huge}%
\setbeamercolor{title}{fg=EPCPpurpleDark}
%
\setbeamerfont{author}{size=\large}%
\setbeamercolor{author}{fg=EPCPpurpleDark}
%
\setbeamercolor{frametitle}{fg=black}%
\setbeamercolor{footline}{fg=white,bg=EPCPpurpleMedium}
\setbeamercolor{itemize item}{fg=EPCPpurple}
\setbeamercolor{enumerate item}{fg=EPCPpurpleDark}
%
\setbeamercolor{block title}{fg=white,bg=EPCPpurpleDark}%
\setbeamercolor{block body}{fg=black,bg=EPCPocher!5}%
\setbeamercolor{block title example}{fg=EPCPocher!5,bg=INRAEgreen}%
\setbeamercolor{block body example}{fg=EPCPpurple,bg=INRAEgreen!5}%
\setbeamercolor{block title alerted}{fg=red!50!black,bg=piggyred}%
\setbeamercolor{block body alerted}{fg=black,bg=white}%
%
\setbeamercolor{frametitle}{fg=black}%
%
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamertemplate{navigation symbols}{}

%
\usefonttheme{professionalfonts}
\ifluatex
\usepackage{fontspec}
\setmainfont{garamond}
\usepackage{unicode-math}
\setmathfont{garamond}
\else
\usepackage[utf8]{inputenc}
\fi
%

\usepackage[version=4]{mhchem}
\usepackage[square,numbers]{natbib}
\usepackage{pgfplots}
\usepackage{tikz}
% \usepackage{subcaption}
\usepackage{graphicx}

\usepackage{booktabs}
\usepackage{hhline} 
\usepackage{tabularx}


\usetikzlibrary{arrows,shapes, shadows, calc, patterns, decorations.pathmorphing,decorations.markings}
\usetikzlibrary{matrix}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{intersections}


%\makeatletter
\mode<all>{
\ifpdf
\pgfdeclareimage[height=0.2\paperheight]{piggyBac}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=2.3ex]{piggyBacSmall}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=0.05\paperheight]{INRAE}{images/INRAE-purple.png}%
\pgfdeclareimage[height=0.05\paperheight]{MathNum}{images/mathnum-purple.jpg}%
\pgfdeclareimage[height=0.08\paperheight]{Eurip}{images/eurip-purple.jpg}%
\pgfdeclareimage[height=0.11\paperheight]{LPI}{images/LPI-purple.png}%
\pgfdeclareimage[height=2.3ex]{LPISmall}{images/Learning_Planet_Institute_LPI-sm.png}%
%
\else
% TODO? eps version
\fi

\setbeamertemplate{title page}{
\begin{beamercolorbox}[wd=\paperwidth]{EPCP}
\parbox[b]{.12\paperwidth}{\hspace*{.5em}\\[-1ex]\mbox{}}
\hfill
\begin{minipage}[b][0.2\paperheight]{.6\paperwidth}{%
		\begin{center}\LARGE
			Economic Principles in Cell Physiology\\[3mm]
			{\footnotesize \color{EPCPpurpleDark}Paris, July 8--11, 2024\\[-2ex]\mbox{}}
		\end{center}
	}
\end{minipage}
\hfill
\pgfuseimage{piggyBac}
\parbox[b]{.04\paperwidth}{\hspace*{.5em}\\[-1ex]\mbox{}}
\end{beamercolorbox}

\vskip0pt plus 1filll
\begin{center}
{\usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle}\\
\vspace{1cm}
{\usebeamerfont{author}\usebeamercolor[fg]{author}\insertauthor}
\end{center}

\mbox{}
\vskip0pt plus 1filll
\begin{beamercolorbox}[wd=\paperwidth]{title}
  \hspace{1cm}\pgfuseimage{INRAE}\hspace{.5cm}\pgfuseimage{MathNum}
  \mbox{}\hfill
  \pgfuseimage{Eurip}\hspace{.5cm}\pgfuseimage{LPI}\hspace{1cm}\mbox{}\hfill\\[4mm]
\end{beamercolorbox}
}% end title page
%
\setbeamertemplate{footline}{
	\begin{beamercolorbox}[wd=\paperwidth]{footline}
		\hfill
		{\mbox{}\raise1.3ex\hbox{\insertshorttitle\hspace*{1em}\insertframenumber\slash\inserttotalframenumber\hspace*{.2em}}}
		\pgfuseimage{piggyBacSmall}
	\end{beamercolorbox}
}% end footline
}% end mode<all>

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START PRESENTATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
	
{\setbeamercolor{background canvas}{bg=EPCPocher}
\begin{frame}[plain]
	\titlepage
\end{frame}
%\maketitle
}

\section{Steady state flux solutions}

\begin{frame}{A whole-cell perspective on metabolism}
  \begin{columns}
    \begin{column}{0.45\textwidth}
  \begin{tikzpicture}
    \node (n) at (0,0) {Nutrients};
    % \node (m) at (0, -2) {\includegraphics[width=2cm]{figs/Metabolism_diagram.png}};
    \node[draw, minimum height=1.5cm] (m) at (0, -2) {Metabolic network};
    \node (b) at (-1.5, -4) {Biomass}; 
    \node (p) at (1.5, -4) {Byproducts};
    \draw[very thick,->] (n) -- (m); 
    \draw[very thick,->] (m) -- (b); 
    \draw[very thick,->] (m) -- (p); 
  \end{tikzpicture}
    \end{column}
    \begin{column}{0.45\textwidth}
      \textbf{Properties} that a model can try to describe
      \begin{itemize}
      \item Exchange fluxes / biomass production under given environmental conditions
      \item What is the internal network state to achieve certain exchange fluxes?
      \item How do the exchange fluxes / the internal network state react to external / internal perturbations?
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Dimensions of metabolism}
  \begin{block}{General overview}
  \begin{itemize}
  \item \textbf{19\,090} known biochemical compounds (KEGG COMPOUND database)
  \item \textbf{11\,911} known biochemical reactions (KEGG REACTION database)
  \item \textbf{8\,423} known enzymes (BRENDA database)
  \end{itemize}
\end{block}

\begin{block}{Organism specific view (\url{biocyc.org})}
  \begin{tabular}{lcc} \toprule
    Organism & \# of reactions & \# of metabolites \\ \midrule
    \textit{Escherichia coli} & 2\,201 & 2\,967 \\
    \textit{Saccharomyces cerevisiae} & 1\,650 & 1\,160 \\
    \textit{Homo sapiens} & 2\,900 & 2\,121 \\
    \textit{Arabidopsis thaliana} & 3\,193 & 2\,777 \\
    \bottomrule
  \end{tabular}
\end{block}
\end{frame}

\begin{frame}{Reconstruction of metabolic networks from genome data}
  \begin{center}
    \begin{tikzpicture}
      \node (g) at (0,0) {Genome};
      \node (m) at (6,0) {Metabolic network};
      \draw[->,very thick,shorten >=1em,shorten <=1em] (g) -- (m);

      \node (gp) at (0, -3) {\includegraphics[width=5cm]{figs/ecoli-genome-around-adhe.pdf}};
      \node (mp) at (6,-3) {\includegraphics[width=5cm]{figs/metabolism-map-kegg.png} \scriptsize{(\texttt{kegg.jp})}};
    \end{tikzpicture}
  \end{center}
  \begin{enumerate}
  \item Identify genes with enzymatic function (annotation / sequence homology)
  \item Find matching reactions in reaction database
  \item Add a biomass reaction (metabolic building blocks + energy (ATP) turnover)
  \end{enumerate}
  \begin{center}
    \textbf{Genome-scale metabolic network model}
  \end{center}
\end{frame}

\begin{frame}{From metabolic networks to models}
  \begin{center}
    \begin{tikzpicture}
      \begin{scope}[every node/.style={draw,text width=0.33\textwidth,text centered}]
        \node[opacity=0.75] (a) at (0,0) {Molar balancing};
        \node[opacity=0.75] (b) at (0,-2) {Stoichiometric model \\ $\dot x = S v$};
        \node[opacity=0.75] (c) at (-3.5,-4.5) {Kinetic models \\[0.75\baselineskip] $\dot x = S v(x)$};
        \node[opacity=1.0] (d) at (3.5,-4.5) {Constraint based models \\[0.25\baselineskip] $
          \begin{aligned}
            \max &\ J(x,v) \\
            \textnormal{s.t.} &\ \dot x = S v
          \end{aligned}
$};
      \end{scope}
      \begin{scope}[thick]
      \draw[->,opacity=0.7] (a) -- (b);
      \draw[->,opacity=0.7] (b) -- (c);
      \draw[->,opacity=0.7] (b) -- (d);
      \end{scope}
    \end{tikzpicture}
  \end{center}
\end{frame}



\begin{frame}{Flux balance analysis}
  \begin{block}{Constraints applied to the network}
  \begin{itemize}
  \item Intermediate / intracellular metabolites are assumed to be in a quasi-steady state:
    \begin{center}
      flux of producing reactions = flux of consuming reactions
    \end{center}
  \item ``Irreversible'' reactions can only have flux in one direction
  \item Maintenance / housekeeping reactions can be constrained to have a minimum flux value (empirical)
  \item Nutrient uptake (exchange) reactions are constrained according to availability of nutrients in the considered environment
  \end{itemize}
\end{block}
\begin{block}{Optimization principle}
  \begin{itemize}
  \item \textbf{Hypothesis}: Cells regulate fluxes within constraints to achieve an “optimal” configuration from an evolutionary perspective.
  \item In many applications, network solutions that \textbf{maximize flux through the biomass reaction} are taken
  \end{itemize}
\end{block}
\end{frame}


\begin{frame}{Constraints on fluxes}
  \begin{enumerate}
  \item Steady state constraint
    \begin{equation*}
      S v = 0
    \end{equation*}
    \vspace{-\baselineskip}
    \begin{itemize}
    \item Fluxes constrained to \textbf{subspace}
    \end{itemize}
  \item Irreversibility constraints on some fluxes (from thermodynamics/heuristics/empirical evidence)
    \begin{equation*}
      v_i \geq 0, \qquad i \textnormal{ irreversible}
    \end{equation*}
    \vspace{-\baselineskip}
    \begin{itemize}
    \item Fluxes constraint to \textbf{flux cone}
    \end{itemize}
  \item Flux bounds from capacity constraints, maintenance, ...
    \begin{equation*}
      v_{i,min} \leq v \leq v_{i,max}
    \end{equation*}
    \vspace{-\baselineskip}
    \begin{itemize}
    \item Fluxes constraint to \textbf{convex polytope}
    \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}{Geometric illustration}
    \begin{tikzpicture}
      \begin{scope}[every node/.style={on grid, text width=0.29\textwidth,text centered}]
    \node (t1) {
      \textbf{Flux space}
};
    \node[right = 0.33\textwidth of t1] (t2) {
      \textbf{Flux cone}
};
    \node[right = 0.33\textwidth of t2] (t3) {
      \textbf{Flux polytope}
};
    \node[text centered,below = 0.3\textheight of t1,draw=structure!40!black,minimum height=0.37\textheight] (b1) {
      \includegraphics[width=3cm]{figs/flux-space.pdf}
};
    \node[right = 0.33\textwidth of b1,text centered,draw=structure!40!black,minimum height=0.37\textheight] (b2) {
       \includegraphics[width=3cm]{figs/flux-cone.pdf}
};
    \node[right = 0.33\textwidth of b2,text centered,draw=structure!40!black,minimum height=0.37\textheight] (b3) {
       \includegraphics[width=3cm]{figs/flux-polytope.pdf}
};
      \end{scope}
  \end{tikzpicture}
\end{frame}

\begin{frame}{Flux space $\rightarrow$ cone $\rightarrow$ polytope example}
  \begin{center}
    \begin{tikzpicture}
      \node (m) at (0,0) {Metabolite};
      \node (c1) at (-4,0) {}; 
      \node (c2) at (4,0) {}; 
      \node (c3) at (0,-2) {};
      \draw[thick,->] (c1) to node[above]{$v_1$} (m); 
      \draw[thick,->] (m) to node[above]{$v_2$} (c2); 
      \draw[thick,->] (m) to node[right]{$v_3$} (c3); 
    \end{tikzpicture}
  \end{center}

  \begin{columns}
    \begin{column}{0.65\textwidth}
  Construct the ...
  \begin{itemize}
  \item flux space;
  \item flux cone assuming $v_2$, $v_3 \geq 0$;
  \item flux polytope assuming $v_1 \leq 0.5$.
  \end{itemize}
\end{column}
\begin{column}{0.35\textwidth}
  Molar balancing:
  \begin{equation*}
    \dot x =
    \begin{pmatrix}
      1 & -1 & -1 
    \end{pmatrix}
    \begin{pmatrix}
      v_1 \\ v_2 \\ v_3
    \end{pmatrix}
  \end{equation*}
\end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{block}{Flux space from $S v = 0$}
        \begin{itemize}
        \item Plane defined by \vspace*{-0.4cm}
          \begin{equation*}
            v_1 - v_2 - v_3 = 0
          \end{equation*} \vspace*{-0.5cm}
          \includegraphics[height=\linewidth]{figs/flux-space3d.pdf}
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{block}{Flux cone}
        \begin{itemize}
        \item Add irreversibility \vspace*{-0.4cm}
          \begin{equation*}
            v_2,\, v_3 \geq 0
          \end{equation*} \vspace*{-0.5cm}
          \includegraphics[height=\linewidth]{figs/flux-cone3d.pdf}
        \end{itemize}\vspace*{0.2cm}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{block}{Flux cone}
        \begin{itemize}
        \item Add irreversibility \vspace*{-0.4cm}
          \begin{equation*}
            v_2,\, v_3 \geq 0
          \end{equation*} \vspace*{-0.5cm}
          \includegraphics[height=\linewidth]{figs/flux-cone3d.pdf}
        \end{itemize}\vspace*{0.2cm}
      \end{block}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{block}{Flux polytope}
        \begin{itemize}
        \item Add upper bound(s) \vspace*{-0.4cm}
          \begin{equation*}
            v_1 \leq 0.5
          \end{equation*} \vspace*{-0.5cm}
          \includegraphics[height=\linewidth]{figs/flux-polytope3d.pdf}
        \end{itemize}\vspace*{0.2cm}
      \end{block}
    \end{column}
  \end{columns}


\begin{frame}{Setting up the constraint based model (CBM)}
  % \item Growth rate is usually small compared to metabolic fluxes:
  %   \begin{equation*}
  %     \dot x = S v - \mu x \approx S v
  %   \end{equation*}
  %   \begin{itemize}
  %   \item Typical violation of 1 \% over 2 -- 3 hours (Reimers \& Reimers 2016)
  %   \end{itemize}
  % \item Consider stoichiometry with exchange fluxes:
  %   \begin{equation*}
  %     S = S_{exch}
  %   \end{equation*}
    \begin{block}{Constraint based model useful if non-trivial steady state fluxes exist}
    \begin{itemize}
    \item The steady state equation
      \begin{equation*}
        S v = 0
      \end{equation*}
      should have a non-zero solution $v$ $\Rightarrow$ non-trivial steady state flux space
    \item We need $\mathop{\mathrm{rank}} S < m$; most models have more reactions than metabolites anyway.
    \end{itemize}
      
    \end{block}
  \begin{block}{Metabolite / flux units}
    \begin{itemize}
    \item In CBMs, metabolites are usually considered in \textbf{molar amounts per dry biomass}: $\mathrm{mmol/g}$
    \item Fluxes are then in $\mathrm{mmol/(g h)}$
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Exchange reactions}
  \begin{itemize}
  \item Exchange reactions are added for all metabolites that are either \textbf{consumed} or \textbf{produced} in a metabolic steady state.
  \item They normally involve only \textbf{extracellular} metabolites.
  \item By convention, the reaction \textbf{direction} is towards the outside of the system
  \end{itemize}
        \begin{center}
        \begin{tikzpicture}[thick]
          \node (x1e) at (-1.5,0) {$\species{A_{e}}$};
          \node (x1) at (0,0) {$\species{A_i}$};
          \node (x2) at (1.5,0) {$\species{B_i}$};
          \node (x2e) at (3,0) {$\species{B_{e}}$};
          \draw[->] (x1e) -- (x1);
          \draw[->] (x1) -- (x2);
          \draw[->] (x2) -- (x2e);
          \draw[double,->] (x1e) -- +(0,-1);
          \draw[double,->] (x2e) -- +(0,-1);
          \draw[rounded corners, thick] (-0.5,-0.5) rectangle (2,0.65);
          \draw[dashed, thin] (-2,-0.75) rectangle (3.5,0.8);

          \draw[->] (5,0.5) -- (5.5, 0.5) node[right] {normal reaction}; 
          \draw[double,->] (5,-0.5) -- (5.5, -0.5) node[right] {exchange reaction}; 
        \end{tikzpicture}
      \end{center}
      \begin{block}{Positive vs.\ negative flux on exchange reaction}
        \begin{itemize}
        \item Negative flux = actually goes into the system = \textbf{supply} (consumption) of a metabolite
        \item Positive flux = goes outside of system = \textbf{removal} (production) of a metabolite
        \end{itemize}
      \end{block}
\end{frame}

\begin{frame}{Biomass composition}
  \begin{columns}[c]
    \begin{column}{0.47\textwidth}
      \begin{block}{\textit{E.~coli} biomass composition}
      \includegraphics[width=0.9\linewidth]{figs/biomass-bionumber-108705.pdf}
      
      \footnotesize{Chassagnole \textit{et al.} 2002, via bionumbers.hms.harvard.edu, ID 108705}

      Varies depending on environmental conditions (nutrients, aerobic/anaerobic, growth rate, ...)
      \note{Higher growth rate = more RNA, less protein content}
      \end{block}
    \end{column}
    \begin{column}{0.47\textwidth}
      \begin{block}{Break down to metabolites}
        \begin{itemize}
        \item 20 proteinogenic amino acids
        \item 8 D/R nucleotides
        \item phospholipids
        \item cofactors / vitamins
        \item ATP hydrolysis required for biomass assembly (``\textbf{growth associated maintenance}'' GAM)
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Biomass reaction}
  \begin{block}{}
    \begin{itemize}
    \item Biomass reaction formalizes consumption of metabolites to generate biomass
    \begin{equation*}
      v_{bio}: \qquad \sum_{i=1}^n c_i X_i \rightarrow 1 \textnormal{ g dry biomass}
    \end{equation*}
    \item Based on pre-determined constant biomass composition
    \item Coefficients $c_i$ commonly in mmol / g dry biomass
    \item Unit of $v_{bio}$ becomes $1/\mathrm{h}$: interpretable as dry biomass growth rate $\mu$!
    \end{itemize}
  \end{block}
\end{frame}

% \begin{frame}{Small-scale example with biomass reaction}
%   \begin{columns}[c]
%     \begin{column}{0.47\textwidth}
%       \centering
%       \begin{tikzpicture}[scale=1.2]
%         % \fill[green,opacity=0.3] (-1.9,-0.57) rectangle (1.9,-0.77);
%         % \fill[green,opacity=0.3] (-1.9,-0.95) rectangle (1.9,-1.15);
%         % \fill[blue,opacity=0.3] (-1.9,-1.15) rectangle (1.9,-1.35);
%         \fill[red,opacity=0.3] (-1.9,-1.55) rectangle (1.9,-2.75);
%         \node at (0,0) {\includegraphics[width=4.8cm]{figs/ethanol-network.png}}; % was 4 cm in 1-scale
%       \end{tikzpicture}
%     \end{column}
%   \end{columns}
% \end{frame}

\begin{frame}{Maintenance}
  \begin{itemize}
  \item ``\textbf{Non-growth associated maintenance}'' (NGAM):
    \begin{itemize}
    \item membrane voltage gradients and osmolarity (ion pumps)
    \item movement (flagella)
    \item macromolecule (RNA/protein/carbohydrates) turnover
    \end{itemize}
  \item Energy demand is commonly represented by a single ATP hydrolysis reaction
    \begin{equation*}
      v_{maint}:\qquad \species{ATP} + \species{H_2O} \rightarrow \species{ADP} + \species{Pi} + \species{H^+}
    \end{equation*}
  \item Put as constraint into constraint based model
    \begin{itemize}
    \item $v_{maint} \geq \alpha$ [mmol / (h $\cdot$ g biomass)]
    \item NGAM rate estimates: \textit{E.~coli} 8.4 mmol/g/h; \textit{S.~cerevisiae} 1.0 mmol/g/h
      \note{Minimum maintenance is based on empirical data.}
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{Optimization principle}
  \begin{block}{Constraint based model}
    \begin{equation*}
      \begin{aligned}
        S v &= 0 \\
        v_{i,min} &\leq v_i \leq v_{i,max}
      \end{aligned}
    \end{equation*}
  \end{block}
  \vspace{-0.7\baselineskip}
  \begin{itemize}
  \item \textbf{Underdetermined} system of equalities / inequalities: flux polytope
  \item How do we determine fluxes $v$ that we expect to occur in nature?
  \end{itemize}
  \begin{block}{Add an optimization objective}
    \begin{itemize}
    \item \textbf{Hypothesis:} Cells regulate fluxes within constraints to achieve an ``optimal'' configuration from an evolutionary perspective.
    \end{itemize}
    \begin{equation*}
      \begin{aligned}
        \max &\ J(v) \\
        \textnormal{s.t. } &\ S v = 0 \\
        &\ v_{i,min} \leq v_i \leq v_{i,max}
      \end{aligned}
    \end{equation*}
  \end{block}
\end{frame}

\begin{frame}{Useful objective functions}
\renewcommand{\arraystretch}{2}
\begin{tabularx}{\linewidth}{|p{3cm}||X||X|}
\hhline{-||-||-}
  Type
 &Objective $J(v)$
 &Principle  \\

\hhline{=::=::=}
Biomass yield
&
$\max v_{bio}$
&
\parbox{3cm}{
\small Biomass flux at fixed max.\ substrate uptake}
\\

\hhline{=::=::=}
ATP yield
&
$\max v_{ATP}$
& 
\parbox{3cm}{
\small ATP flux at fixed max.\ substrate uptake
}\\

\hhline{=::=::=}
Minimal flux
&
$\min \Vert v \Vert^2$
& 
\parbox{3cm}{
\small Minimization of overall flux ($\sim$ enzyme usage)
}\\

\hhline{=::=::=}
Biomass flux yield
&
$\max v_{bio} / \Vert v \Vert^2$
& 
\parbox{3cm}{
\small Biomass yield per overall flux unit 
}\\

\hhline{-||-||-}
\end{tabularx}

\vspace{\baselineskip}
\begin{footnotesize}
  Empirical evaluation of objective functions:
Schuetz, R., Kuepfer, L., \& Sauer, U. (2007). Systematic evaluation of objective functions for predicting intracellular fluxes in \textit{Escherichia coli}. Molecular Systems Biology, 3, 119.
\end{footnotesize}
\end{frame}

\begin{frame}{Collections of constraint based models}
  \begin{center}
    \includegraphics[width=8cm]{figs/bigg-models-screenshot.png}
  \end{center}
  \begin{itemize}
  \item   BiGG models database: \url{http://bigg.ucsd.edu/models}
  \item ModelSEED (plant models): \url{https://modelseed.org/genomes/}
  \item BioModels database: \url{https://biomodels.net} (filter for ``constraint-based model'')
  \end{itemize}
\end{frame}

\section{Basics of linear programming}

\begin{frame}{Linear programs}
A linear program in standard form:
  \begin{equation*}
    \begin{aligned}
      \max &\ c\T v \\
      \textnormal{s.t.} &\ A v = b \\
      &\ v \geq 0
    \end{aligned}
  \end{equation*}

  \begin{columns}[b]
    \begin{column}{0.47\textwidth}
  \begin{description}
  \item[\textbf{Objective}] ~\\ $c\T v$
  \item[\textbf{Equality constraint}] ~\\ $A v = b$
  \item[\textbf{Inequality constraint}] ~\\ (Cone constraint) $v \geq 0$
  \end{description}
    \end{column}
    \begin{column}{0.47\textwidth}
      \begin{tikzpicture}[thick]
        \draw[->] (-1.5,0) -- (2.5,0);
        \draw[->] (0,-1.5) -- (0,2.5);
        \draw[dashed, very thick, red!60!black] (-0.5,2.5) -- node[pos=0.3,above right] {$A v = b$} (2.5,-0.5);
        \draw[ultra thick, red!60!black] (0,2) -- (2,0);
        \draw[very thick,blue] (-1.5,0.5) -- node[pos=0.5,below] (m) {$c\T v$} (2.5,-0.5);
        \draw[blue,->] (m) -- +(0.3,1.2);
      \end{tikzpicture}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Example}
  \begin{columns}[b]
    \begin{column}{0.47\textwidth}
  \begin{equation*}
    \begin{aligned}
      \max_{v_1, v_2} &\ v_2 \\
      \textnormal{s.t.} &\ v_1 + v_2 = 1 \\
      &\ v \geq 0
    \end{aligned}
  \end{equation*}

  Thus:
  \begin{equation*}
    \begin{aligned}
      c\T &=
      \begin{pmatrix}
        0 & 1
      \end{pmatrix} \\
      A &=
      \begin{pmatrix}
        1 & 1
      \end{pmatrix} \\
      b &= 1
    \end{aligned}
  \end{equation*}
    \end{column}
    \begin{column}{0.47\textwidth}
      \begin{tikzpicture}[thick]
        % \fill[red, opacity=0.2] (0,0) -- (2,0) -- (0,2) -- (0,0);
        \draw[->] (-1.5,0) -- (2.5,0);
        \draw[->] (0,-1.5) -- (0,2.5);
        \draw[dashed, very thick, red!60!black] (-0.5,2.5) -- node[pos=0.3,above right] {$v_1 + v_2 = 1$} (2.5,-0.5);
        \draw[ultra thick, red!60!black] (0,2) -- (2,0);
        \draw[very thick,blue] (-1.5,0.5) -- node[pos=0.5,below] (m) {$
          \begin{pmatrix}
            0 \\ 1
          \end{pmatrix}
$} (2.5, 0.5);
        \draw[blue,->] (m) -- +(0,1.2);
      \end{tikzpicture}
    \end{column}
  \end{columns}
\end{frame}

% \begin{frame}{Slack variables}
%   \begin{itemize}
%   \item Different optimization problems can be transformed into an LP
%   \item Example: flux polytope
%     \begin{equation*}
%       \begin{aligned}
%         S v &= 0 \\
%         0 &\leq v_i \leq v_{i,max}
%       \end{aligned}
%     \end{equation*}
%   \item Slack variables: introduce additional non-negative variables $z_i$ to ``match'' inequality constraints
%     \begin{equation*}
%       v_i \leq v_{i,max} \qquad \longrightarrow \qquad
%       \begin{aligned}
%         v_i + z_i &= v_{i,max} \\
%         z_i &\geq 0
%       \end{aligned}
%     \end{equation*}
%   \item Original problem has been transformed into a standard form LP through the introduction of slack variables
%     \note{Typically LP solvers do this automatically, you can specify any mixture of affine equality / inequality constraints}
%   \end{itemize}
% \end{frame}

\begin{frame}{Generalized geometrical interpretation}
\begin{center}
    \begin{tikzpicture}[thick]
      \fill[blue!30] (0,0) -- +(20:5cm) |- +(70:5cm) -- cycle;
      \draw[blue] (0,0) -- +(20:5cm) |- +(70:5cm) -- cycle;
      \draw[red!60!black,very thick] (0,4) -- node[pos=0.5,below left] (m) {$c\T v$} (5,1);
      \draw[red!60!black,->] (m) -- +(0.75,1.25);
      \coordinate (opt) at (20:5cm |- 70:5cm);
      \node[above right,red!60!black] at (opt) {$v^\ast$};
      \draw[red!60!black,very thick] (opt) -- +(-2,1.2);
      \draw[red!60!black,very thick] (opt) -- +(1,-0.6);
    \end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}{Non-uniqueness of optimal solutions}
\begin{center}
    \begin{tikzpicture}[thick]
      \fill[blue!30] (0,0) -- +(20:5cm) |- +(70:5cm) -- cycle;
      \draw[blue] (0,0) -- +(20:5cm) |- +(70:5cm) -- cycle;
      \draw[red!60!black,very thick] (0,2) -- node[pos=0.5,below left] (m) {$c\T v$} (5,2);
      \draw[red!60!black,->] (m) -- +(0,1.5);
      \coordinate (opt) at (20:5cm |- 70:5cm);
      \draw[ultra thick,red!60!black] (70:5cm) -- node[pos=0.5,above] {$v^\ast$} (opt);
    \end{tikzpicture}
\end{center}
\begin{block}{}
  Set of optimal solutions is a \textbf{face of the polytope}
\end{block}
\end{frame}

\begin{frame}{Unboundedness}
  \begin{center}
    \begin{tikzpicture}[thick]
      \fill[blue!30] (0,0) -- +(20:5cm) arc[start angle=20, delta angle=50, radius=5cm] -- cycle;
      \draw[->,blue] (0,0) -- +(20:5cm);
      \draw[->,blue] (0,0) -- +(70:5cm);
      \draw[red!60!black,very thick] (0,4) -- node[pos=0.5,below left] (m) {$c\T v$} (5,1);
      \draw[red!60!black,->] (m) -- +(0.75,1.25);
    \end{tikzpicture}
  \end{center}

\textbf{Unboundedness:} $\max c\T v = \infty$
\end{frame}

\begin{frame}{Infeasibility: Constraint set is empty}
\begin{center}
    \begin{tikzpicture}[thick]
      \fill[blue!10] (0,0) -- +(20:5cm) |- +(70:5cm) -- cycle;
      \fill[white] (0.5,0.5) -- +(20:4cm) |- +(70:4cm) -- cycle;
      \draw[blue] (0,0) -- +(20:5cm) |- +(70:5cm) -- cycle;
      \fill[blue!10] (6,0) -- (7,5) -- +(0.5,0) -- (6.5,0) -- cycle;
      \draw[blue] (6,0) -- (7,5);
    \end{tikzpicture}
  \end{center}
  \begin{block}{Example}
    \begin{equation*}
      \begin{aligned}
        v_1 + v_2 &\leq -1 \\
        v_1, v_2 &\geq 0
      \end{aligned}
    \end{equation*}
  \end{block}
\end{frame}

\section{Flux balance analysis}

\begin{frame}{Flux balance analysis (FBA)}
  \begin{block}{FBA to maximize biomass yield as LP}
    \begin{equation*}
      \begin{aligned}
      J^\ast = \max &\ v_{bio} \\
      \textnormal{s.t.} &\ S v = 0 \\
      &\ v_{i,min} \leq v_i \leq v_{i,max}
      \end{aligned}
    \end{equation*}
  \end{block}
  \begin{itemize}
  \item Typical relevant constraint is glucose / oxygen uptake rate
    \begin{equation*}
      - v_{e,gluc,max} \leq v_{e,gluc} \leq 0
    \end{equation*}
  \item For practical reasons $v_{i,max} = M$ ($10^6$ mole/h/g) even if no capacity constraint
  \item Typically no unique optimal flux distribution $v^\ast$
  \end{itemize}
\end{frame}

% \begin{frame}{Shadow prices in FBA}
%   \begin{itemize}
%   \item Sensitivity of the optimal solution with respect to molar balance constraints $S v = 0$
%     \begin{equation*}
%       \frac{\partial p^\ast}{\partial b_j} = y_j^\ast
%     \end{equation*}
%     \begin{block}{}
%       How much more biomass / ATP / product can the organism make if it receives a ``free'' additional source of metabolite $j$?
%     \end{block}
%   \item Shadow prices of limiting substrates are often related to their yield with respect to biomass formation.
%   \end{itemize}
% \end{frame}

\begin{frame}{FBA example: \textit{E.~coli} core}
  \begin{itemize}
  \item Core carbon network from BiGG database: 72 metabolites, 95 reactions
  \item Network visualization from \url{https://escher.github.io/}
  \end{itemize}
  \begin{center}
    \includegraphics[width=8cm]{figs/ecc-escher.png}
  \end{center}
\end{frame}

\begin{frame}{FBA results: comparing intracellular flux states}
  \begin{itemize}
  \item With a graphical layout of the metabolic network is available: \textbf{graphical illustration of intracellular metabolic state}
  \end{itemize}
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \begin{block}{Aerobic growth in \textit{E.\ coli} core}
        \includegraphics[width=0.9\linewidth]{figs/ecc_aerobic.png}
      \end{block}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{block}{Anaerobic growth in \textit{E.\ coli} core}
        \includegraphics[width=0.9\linewidth]{figs/ecc_escher_anaerobic.png}
      \end{block}
    \end{column}
  \end{columns}
  \begin{flushright}
    \footnotesize Made with \url{escher.github.io}
  \end{flushright}
\end{frame}



% \begin{frame}{Flux variability analysis (FVA)}
%   \begin{block}{}
%     What are maximum / minimum fluxes achievable within an ``optimal'' flux distribution?
%   \end{block}
%   \begin{enumerate}
%   \item Compute objective for optimal flux distribution
%     \begin{equation*}
%       \begin{aligned}
%       J^\ast = \arg \max &\ c\T v \\
%       \textnormal{s.t. } & ...
%       \end{aligned}
%     \end{equation*}
%   \item Put objective as constraint and optimize individual fluxes
%     \begin{equation*}
%       \begin{aligned}
%       \bar v_j = \arg \max / \min &\ v_j \\
%       \textnormal{s.t. } &\ S v = 0 \\
%       &\ v_{i,min} \leq v_i \leq v_{i,max} \\
%       &\ {\color{red!60!black}c\T v \geq (1 - \varepsilon) J^\ast}
%       \end{aligned}
%     \end{equation*}
%   \end{enumerate}
% \end{frame}

% \begin{frame}{Product yield vs.\ biomass yield}
%   \begin{center}
%           \begin{tikzpicture}[very thick]
%         \node (s) at (0,0) {Substrate};
%         \node (b) at (3,0.4) {Biomass};
%         \node (p) at (3,-0.4) {Product};
%         \draw[->] (s) -- (p) coordinate[pos=0.5] (sp);
%         \draw[->] (s) -- (b) coordinate[pos=0.5] (sb);
%         \draw[->,dotted,shorten >=2pt] (b) to[out=-130,in=40] (sp);
%         \draw[->,dotted,shorten >=2pt] (b) to[bend right] (sb);
%         \node (lb) at (-1, 0) {};
%         \node (rb) at (4, 0) {};
%       \end{tikzpicture}
%   \end{center}
%   \begin{itemize}
%   \item Extracellular biochemical product $\species{P_e}$, with exchange reaction
%     \begin{equation*}
%       v_{prod}: \species{P_e} \longrightarrow
%     \end{equation*}
%     \vspace{-0.8\baselineskip}
%     \begin{block}{Minimize / maximize $v_{prod}$ at fixed biomass flux $\bar v_{bio}$}
%     \vspace{-0.3\baselineskip}
%       \begin{equation*}
%         \begin{aligned}
%           \max &\ \pm v_{prod} \\
%           \textnormal{s.t. } &\ S v = 0 \\
%           &\ v_{i,min} \leq v_i \leq v_{i,max} \\
%           &\ v_{bio} = \bar v_{bio}
%         \end{aligned}
%       \end{equation*}
%     \end{block}
%   \end{itemize}
% \end{frame}

% \begin{frame}{Production envelopes}
%   % \begin{center}
%   %   \includegraphics[width=6cm]{figs/yield-tradeoff-maranas2016.pdf}
%   % \end{center}
%   % \begin{flushright}
%   %   \footnotesize [Maranas \& Zomorrodi, 2016]
%   % \end{flushright}

%   \begin{center}
%     \begin{tikzpicture}
%       \draw[->,thick] (0,0) -- (7,0) node[below left]{$v_{bio}$};
%       \draw[->,thick] (0,0) -- (0,5) node[below left]{$v_{prod}$};
%       \begin{scope}[every path/.style={red!80!black, very thick}]
%         \draw[name path=envelope] (0,4) -- (4,3.5) -- (5.5, 1.5) -- (3,0) -- (0,0);
%         \draw[opacity=0.0,name path=mu1] (0,0) -- (0,5);
%         \fill[red!80!black,name intersections={of=envelope and mu1}] (intersection-1) circle(3pt) (intersection-2) circle(3pt);
%         \draw[opacity=0.0,name path=mu2] (1,0) -- (1,5);
%         \fill[red!80!black,name intersections={of=envelope and mu2}] (intersection-1) circle(3pt) (intersection-2) circle(3pt);
%         \draw[opacity=0.0,name path=mu3] (2,0) -- (2,5);
%         \fill[red!80!black,name intersections={of=envelope and mu3}] (intersection-1) circle(3pt) (intersection-2) circle(3pt);
%         \draw[opacity=0.0,name path=mu4] (3,0) -- (3,5);
%         \fill[red!80!black,name intersections={of=envelope and mu4}] (intersection-1) circle(3pt) (intersection-2) circle(3pt);
%         \draw[opacity=0.0,name path=mu5] (4,0) -- (4,5);
%         \fill[red!80!black,name intersections={of=envelope and mu5}] (intersection-1) circle(3pt) (intersection-3) circle(3pt);
%         \draw[opacity=0.0,name path=mu5] (5,0) -- (5,5);
%         \fill[red!80!black,name intersections={of=envelope and mu5}] (intersection-1) circle(3pt) (intersection-2) circle(3pt); 
%         \fill[red!80!black] (5.5,1.5) circle(3pt);
%       \end{scope}
%     \end{tikzpicture}
%   \end{center}

%   \begin{enumerate}
%   \item Fix growth rate ($v_{bio}$) at different values
%   \item Compute maximum / minimum for product flux for each growth rate by FVA
%   \end{enumerate}
% \end{frame}

\begin{frame}{Dynamic FBA: general idea}
  \begin{itemize}
  \item Put FBA models in a dynamic context (biomass growth, nutrient consumption)
  \item Starting from a mass balancing model like the Monod model:
    \begin{equation*}
        \begin{aligned}
        \frac{dX}{dt} &= \mu(c) X \\
        \frac{dc}{dt} &= - \frac{\mu(c)}{Y_{X/c}} X
      \end{aligned}
    \end{equation*}
  \item replace the growth rate $\mu(c)$ by an ``optimal'' growth rate from FBA model
  \item replace the substrate / product rates by exchange fluxes from FBA model
  \end{itemize}
  \begin{block}{Key steps / questions}
    \begin{itemize}
    \item How do we set the reaction constraints (mostly transport capacity) based on the changing nutrient availability?
    \item Connect the FBA-based part (optimization problem) to the dynamic part (differential equation model)
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Integrating the DFBA model parts}
  %\vspace*{-2cm}
  \begin{center}
    \includegraphics[width=10cm]{figs/dFBA-scheme.pdf}
  \end{center}
  \begin{flushright}
    \footnotesize EPCB book, chapter ``Optimal cell behavior in time''
  \end{flushright}
  %\vspace*{-2cm}
  \begin{itemize}
  \item Enzyme kinetics for bounds $v_{i,min}(c)$, $v_{i,max}(c)$: usually only a couple of (uptake) reactions
  \item Optimal growth rate \& exchange fluxes from FBA model are used in dynamic equations
  \end{itemize}
\end{frame}

\begin{frame}{DFBA: Example with \textit{E.\ coli} core model}
  \begin{center}
    \includegraphics[width=0.82\linewidth]{figs/dfba-example.pdf}
  \end{center}
  \begin{flushright}
    \footnotesize EPCB book, chapter ``Optimal cell behavior in time''
  \end{flushright}
\end{frame}

\begin{frame}{Outlook: further extensions of FBA}
  \begin{itemize}
  \item Thermodynamic FBA
  \item Resource allocation models:
    \begin{itemize}
    \item ME models
    \item Resource balance analysis
    \item Dynamic enzyme-cost FBA
    \end{itemize}
  \end{itemize}

  \begin{block}{Exercise on \url{https://principlescellphysiology.org/book-economic-principles/index.html}}
    Run FBA on the carbon core model (Jupyter notebook $\rightarrow$ Google Colaboratory)
  \end{block}
\end{frame}




% \begin{frame}{Frame Title}
% \begin{itemize}
% 	\item Test font shapes\slash series: normal, \emph{emph}, \textbf{bold}
% 	\item Test formulas $\displaystyle
% 	\int_0^1 f(x) dx = F(1) - F(0)
% 	$
% 	\item Test newly defined colors\\
% {\color{piggyred}\rule{1cm}{1cm}}
% {\color{EPCPpurple}\rule{1cm}{1cm}}
% {\color{EPCPpurpleDark}\rule{1cm}{1cm}}
% {\color{EPCPocher}\rule{1cm}{1cm}}
% {\color{INRAEgreen}\rule{1cm}{1cm}}
% \end{itemize}

% \begin{enumerate}
% 	\item Test
% 	\item Enumerate environment
% \end{enumerate}
% \end{frame}



% \begin{frame}\frametitle{A Frame Title is Important.}
% \framesubtitle{Subtitles are not so important.}

% \begin{block}{test block}
% 	content
% \end{block}

% \begin{exampleblock}{test exampleblock}
% 	content
% \end{exampleblock}

% \begin{alertblock}{test alertblock}
% 	content
% \end{alertblock}
% \end{frame}

\end{document}

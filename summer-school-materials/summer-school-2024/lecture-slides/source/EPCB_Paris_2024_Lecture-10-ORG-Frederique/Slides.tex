\documentclass[aspectratio=169]{beamer}

\usepackage{ifluatex}
%
\title{Economy of organ form and function}
\author{Fr\'ed\'erique No\"el}
\date{July 10, 2024}

%
\definecolor{piggyred}{RGB}{255,212,213}
\definecolor{EPCPpurple}{RGB}{105,88,153}
\definecolor{EPCPpurpleMedium}{RGB}{159,139,213}
\definecolor{EPCPpurpleDark}{RGB}{31,0,81}
\definecolor{EPCPocher}{RGB}{254,244,254}
\definecolor{INRIAgreen}{RGB}{0,163,166}

\setbeamercolor{EPCP}{fg=white,bg=EPCPpurpleMedium}
\usecolortheme[named=EPCPpurple]{structure}
%
\setbeamerfont{title}{size=\huge}%
\setbeamercolor{title}{fg=EPCPpurpleDark}
%
\setbeamerfont{author}{size=\large}%
\setbeamercolor{author}{fg=EPCPpurpleDark}
%
\setbeamercolor{frametitle}{fg=black}%
\setbeamercolor{footline}{fg=white,bg=EPCPpurpleMedium}
\setbeamercolor{itemize item}{fg=EPCPpurple}
\setbeamercolor{enumerate item}{fg=EPCPpurpleDark}
%
\setbeamercolor{block title}{fg=white,bg=EPCPpurpleDark}%
\setbeamercolor{block body}{fg=black,bg=EPCPocher!5}%
\setbeamercolor{block title example}{fg=EPCPocher!5,bg=INRIAgreen}%
\setbeamercolor{block body example}{fg=EPCPpurple,bg=INRIAgreen!5}%
\setbeamercolor{block title alerted}{fg=red!50!black,bg=piggyred}%
\setbeamercolor{block body alerted}{fg=black,bg=white}%
%
\setbeamercolor{frametitle}{fg=black}%
%
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamertemplate{navigation symbols}{}

%
\usefonttheme{professionalfonts}
\ifluatex
\usepackage{fontspec}
%\setmainfont{garamond}
\usepackage{unicode-math}
%\setmathfont{garamond}
\else
\usepackage[utf8]{inputenc}
\fi
%

\usepackage[version=4]{mhchem}
\usepackage[square,numbers]{natbib}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{subcaption}
\usepackage{graphicx}

%\makeatletter
\mode<all>{
\ifpdf
\pgfdeclareimage[height=0.2\paperheight]{piggyBac}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=2.3ex]{piggyBacSmall}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=0.05\paperheight]{INRIA}{images/inria-logo.jpg}%
\pgfdeclareimage[height=0.05\paperheight]{MathNum}{images/mathnum-purple.jpg}%
\pgfdeclareimage[height=0.08\paperheight]{Eurip}{images/eurip-purple.jpg}%
\pgfdeclareimage[height=0.11\paperheight]{LPI}{images/LPI-purple.png}%
\pgfdeclareimage[height=2.3ex]{LPISmall}{images/Learning_Planet_Institute_LPI-sm.png}%
%
\else
% TODO? eps version
\fi

\setbeamertemplate{title page}{
\begin{beamercolorbox}[wd=\paperwidth]{EPCP}
\parbox[b]{.12\paperwidth}{\hspace*{.5em}\\[-1ex]\mbox{}}
\hfill
\begin{minipage}[b][0.2\paperheight]{.6\paperwidth}{%
		\begin{center}\LARGE
			Economic Principles in Cell Physiology\\[3mm]
			{\footnotesize \color{EPCPpurpleDark}Paris, July 8--11, 2024\\[-2ex]\mbox{}}
		\end{center}
	}
\end{minipage}
\hfill
\pgfuseimage{piggyBac}
\parbox[b]{.04\paperwidth}{\hspace*{.5em}\\[-1ex]\mbox{}}
\end{beamercolorbox}

\vskip0pt plus 1filll
\begin{center}
{\usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle}\\
\vspace{1cm}
{\usebeamerfont{author}\usebeamercolor[fg]{author}\insertauthor}
\end{center}

\mbox{}
\vskip0pt plus 1filll
\begin{beamercolorbox}[wd=\paperwidth]{title}
  \hspace{1cm}\pgfuseimage{INRIA}\hspace{.5cm}\pgfuseimage{MathNum}
  \mbox{}\hfill
  \pgfuseimage{Eurip}\hspace{.5cm}\pgfuseimage{LPI}\hspace{1cm}\mbox{}\hfill\\[4mm]
\end{beamercolorbox}
}% end title page
%
\setbeamertemplate{footline}{
	\begin{beamercolorbox}[wd=\paperwidth]{footline}
		\hfill
		{\mbox{}\raise1.3ex\hbox{\insertshorttitle\hspace*{1em}\insertframenumber\slash\inserttotalframenumber\hspace*{.2em}}}
		\pgfuseimage{piggyBacSmall}
	\end{beamercolorbox}
}% end footline
}% end mode<all>

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START PRESENTATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

{\setbeamercolor{background canvas}{bg=EPCPocher}
\begin{frame}[plain]
	\titlepage
\end{frame}
%\maketitle
}

\begin{frame}\frametitle{Table of contents}
	\tableofcontents
\end{frame}

\section{Organ morphogenesis}

\begin{frame}
	\begin{center}
		\Huge{ \textcolor{EPCPpurpleDark}{Organ morphogenesis}}
	\end{center}
\end{frame}

\subsection{Optimization}

\begin{frame}\frametitle{Constraints}

Organ development in pluricellulars is submitted to constraints:

\begin{table}[]
\begin{tabular}{ccc}
Function & Shape \\ 
\includegraphics[height=0.45\paperheight]{images/heart.jpg} & \includegraphics[height=0.45\paperheight]{images/sketch_bronchi.png}
\end{tabular}
\end{table}

Minimization of energy while satisfying the constraints
	
\end{frame}

\begin{frame}\frametitle{Optimization}

The perfect organ does not exist. But the optimal can be reached.

\begin{block}{Mathematical framework}
	\begin{itemize}
		\item Cost function $\mathcal{E}$ dependent on one or several variables $x \in \mathbb{R}^n$
		\item One or several equality constraints: $c(x) = 0$, where $c \, : \, \mathbb{R}^n \to \mathbb{R}^m$
		\item Find an optimal value $x^*$ that minimizes the function $\mathcal{E}(x)$ while $c(x^*) = 0$
	\end{itemize}
\end{block}

\end{frame}

\subsection{The example of the lung}

\begin{frame}\frametitle{The example of the lung}

	\begin{columns}
		\begin{column}{0.65\textwidth}
			\begin{block}{Problematics}
				\begin{itemize}
					\item Role: connects O$_2$ and CO$_2$ in atmosphere with inner body 
			
					\item Medium: gas transfer by diffusion through alveolar membrane
			
					\item Major constraints:
					
						\begin{itemize}
							\item Diffusion: a surface process
							\item Limited thoracic volume
						\end{itemize}
			
				\end{itemize}
			\end{block}

			\begin{block}{Solution}
				Optimize (maximize) the surface/volume ratio!
			\end{block}
		\end{column}

		\begin{column}{0.35\textwidth}

			\begin{table}[]
				\begin{tabular}{c}
				 \includegraphics[height=0.3\paperheight]{images/sketch_bronchi_acini.png} \\
				 \includegraphics[height=0.25\paperheight]{images/gas_alveolus.png}
				\end{tabular}
				\end{table}
		\end{column}
	\end{columns}

\end{frame}

	
\begin{frame}\frametitle{Lung morphometry}


	\begin{block}{Characteristics necessary for a proper functioning of the lung}
		\begin{itemize}
			\item Space-filling
			\item Self-avoiding 
		\end{itemize}
	\end{block}
	
		\begin{table}[]
				\begin{tabular}{cc}
				\includegraphics[height=0.4\paperheight]{images/trumpet_cumulated.png} &
				\includegraphics[height=0.4\paperheight]{images/lung_fractal_mandelbrot} 
				\end{tabular}
				\end{table}

	\end{frame}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\begin{frame}\frametitle{Lung morphogenesis}
	
	\begin{block}{Two types of approaches}
		\begin{itemize}
			\item Programmed morphogenesis
			\item Self-organized morphogenesis
		\end{itemize}
	\end{block}
	
	\begin{table}[]
	\begin{tabular}{cc}
	\includegraphics[height=0.4\paperheight]{images/metzger_pdf.pdf} & \includegraphics[height=0.38\paperheight]{images/fig_fgf10_png.png} \\
	\end{tabular}
	\end{table}
	
	\end{frame}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\begin{frame}\frametitle{Lung morphogenesis}
	
	\begin{table}[]
	\begin{tabular}{c}
	\includegraphics[height=0.55\paperheight]{images/TreeGrowth3D_crop.png} \\
	\end{tabular}
	\end{table}
	
	\begin{center}
		{\footnotesize Rendered image based on simulations from Cl\'ement et al., 2014}
	\end{center}
	
	\end{frame}
	
	\section{The lung as a model organ for optimization under constraints} % (fold)
	\label{sec:the_lung_as_a_model_organ_for_optimization_under_constraints}
	
	\begin{frame}
		\begin{center}
			\Huge{ \textcolor{EPCPpurpleDark}{The lung as a model organ for optimization under constraints}}
		\end{center}
	\end{frame}
	
	\subsection{Lung morphology}
	
	\begin{frame}\frametitle{Lung morphology}
		\begin{columns}
		  \begin{column}{0.6\textwidth}
			  \begin{block}{Bronchial tree}
				  \begin{itemize}
					\item Cascade of bifurcating airways with cylindrical shapes
					\item Around 17 generations
					\item Size of the airways decreases at each bifurcation
				  \end{itemize}
			  \end{block}
			  
			  \begin{block}{Acini}
				  \begin{itemize}
					  \item Exchange surface with blood ($70-100 \, \text{m}^2$ )
					\item Alveoli: bubble-like structure 
					\item Around 6 generations
				  \end{itemize}
			  \end{block}
		  \end{column}
			\begin{column}{0.4\textwidth}
		  \begin{figure}
		  \includegraphics[scale=0.35]{images/poumon}
		  \caption{Cast of the human's lung made by E.R. Weibel}
		  \end{figure}
			\end{column}
		 \end{columns}
	\end{frame}
	
	\begin{frame}\frametitle{Modelling the human lung}
		\begin{columns}
		  \begin{column}{0.6\textwidth}
			  \begin{block}{Assumptions}
				  \begin{itemize}
					\item Symmetric dichotomic bifurcating tree.
					\item Branches are assumed to be cylindrical.
					\item Size of the bronchi of generation $i$: \[
					 \begin{split}
						 l_{i+1} =  l_{i}h \Rightarrow l_i = l_0h^i, \\
						r_{i+1} =  r_{i}h \Rightarrow r_i = r_0h^i,
					 \end{split}
					\]
					\item Homothetic ratio between generations. 
					\begin{equation*}
					 h =  \left\{
						  \begin{aligned}
							& 2^{-1/3} \, \text{ in the bronchial tree}, \\
							& 1 \, \text{ in the acinus}.
						  \end{aligned}
						\right.
					\end{equation*}
				  \end{itemize}
			  \end{block}
			  
			  
		  \end{column}
			\begin{column}{0.4\textwidth}
		  \begin{figure}
		  \includegraphics[scale=0.5]{images/tree-crop}
		  \caption{Illustration of the lung model}
		  \end{figure}
			\end{column}
		 \end{columns}
	\end{frame}
	
	\subsection{Lung dynamics}
	
	\begin{frame}\frametitle{Diffusion process}
		\begin{block}{Diffusion}
			\begin{itemize}
				\item Passive process 
				\item Balance the partial pressures between blood and the alveolar air
			\end{itemize}
		\end{block}
		
		\begin{block}{Limitations}
			\begin{itemize}
				\item Pathways from the ambient air to the respiratory zone are too long ($L_p \approx 30$ cm) 
				\item Characteristic time to travel by diffusion: 
			$$ t_p = \frac{L_p}{D} \approx 4500 \text{ s } = 1 \text{ hour and } 15 \text{ minutes !}$$
			\end{itemize}
		\end{block}
		
	\end{frame}
	
	\begin{frame}\frametitle{Convection process}
	
			 \begin{block}{Ventilation}
				 \begin{itemize}
					 \item Dynamic process
					 \item Air of the lung renewed
					 \item Performed thanks to a set of muscles (ex. diaphragm)
					 \item Two phases: inspiration and expiration
				 \end{itemize}
			 \end{block}
		 \begin{figure}
			 \includegraphics[scale=0.3]{images/ventilation}
		 \end{figure}
	\end{frame}
	
	\begin{frame}\frametitle{Modelling oxygen transport}
	
	\begin{columns}
		\begin{column}{0.7\textwidth}
		Convection-diffusion-reaction equation in each airway 
		\begin{equation*}
			\frac{\partial P}{\partial t} - D \frac{\partial^2 P}{\partial x^2} + u(t)\frac{\partial P}{\partial x} = \beta \left( P_{\text{blood}} - P\right)
		\end{equation*}
		
		\bigskip 
		
		Link all generations by assuming:
		\begin{itemize}
			\item Continuity between generations
			\item Conservation of the quantity of oxygen
		\end{itemize}
		
		\end{column}
		
		\begin{column}{0.3\textwidth}
		\begin{figure}
		\includegraphics[scale=0.4]{images/TreeModel}
		\end{figure}
		\end{column}
		\end{columns}
		
		% Exchange coefficient $\beta$ depends on :
	% 	\begin{itemize}
	% 		\item the generation of the airway
	% 		\item the diffusion coefficient of oxygen in water
	% 		\item the solubility coefficient of oxygen in water
	% 		\item the thickness of the alveolar-capillary membrane
	% 		\item the radius of the alveolar duct
	% 	\end{itemize}
	\end{frame}
	
	\begin{frame}\frametitle{Numerical simulations}
		\begin{columns}
			\begin{column}{0.5\textwidth}
				\begin{block}{Inputs}
					\begin{itemize}
						\item Tidal volume
						\item Breathing frequency
					\end{itemize}
				\end{block}
			\end{column}
			
			\begin{column}{0.5\textwidth}
				\begin{block}{Outputs}
					\begin{itemize}
						\item $O_2$ flow to blood
						\item $CO_2$ flow to blood
					\end{itemize}
				\end{block}
			\end{column}
		\end{columns}
		
		\begin{columns}
			\begin{column}{0.3\textwidth}
				$$ \dot{V}_{O_2} = 230 \text{ mL} \cdot \text{min}^{-1} $$
				$$ \dot{V}_{CO_2} = 180 \text{ mL} \cdot \text{min}^{-1}$$
			\end{column}
			
			\begin{column}{0.7\textwidth}
				\begin{figure}
					\includegraphics[scale=0.4]{images/FluxO2}
				\end{figure}
			\end{column}
		\end{columns}
	\end{frame}
	
	
	\subsection{Energetic cost of breathing}
	
	\begin{frame}\frametitle{Power spent during ventilation}
		\begin{columns}
			\begin{column}{0.55\textwidth}
				Action of the muscles on the lung:
						\begin{itemize}
							\item Deforms the tissues
							\item Displaces the air along the bronchial tree
						\end{itemize}
						
						\bigskip
						
					$$
					\underbrace{\mathcal{P}_{\rm{m}}}_{\text{muscle power}} \simeq \underbrace{\mathcal{P}_{\rm{e}}}_{\text{elastic power}} + \underbrace{\mathcal{P}_{\rm{a}}}_{\text{air viscous dissipation}}
					$$
			\end{column}
			
			\begin{column}{0.5\textwidth}
				\begin{figure}
					\includegraphics[scale=0.3]{images/Power}
				\end{figure}
			\end{column}
		\end{columns}
		
	\end{frame}
	
	\begin{frame}\frametitle{Power spent during ventilation}
		
		\begin{block}{Viscous dissipation of air}
			\begin{itemize}
				\item Characterized by the lung hydrodynamic resistance 
					\begin{itemize}
						\item Connects the airflow $\mathcal{F}$ to the air pressure $p$: $ p = \mathcal{F} \mathcal{R} $
					\end{itemize}
				\item Power dissipated
					$$ \mathcal{P}_{\rm{a}} = \mathcal{R} \mathcal{F}^2 = \frac{1}{4}\mathcal{R}(\pi f_b V_T)^2$$
			\end{itemize}
			
			
		\end{block}
		
		\begin{block}{Elastic power}
			\begin{itemize}
				\item Characterized by the compliance of the lung 
					\begin{itemize}
						\item Relates the force per unit of surface applied by the muscles to the volume change of the lung
					\end{itemize}
				\item Elastic power
				$$\mathcal{P}_{\rm e} = \frac{V_T^2 f_b}{2 \mathcal{C}}$$
			\end{itemize}
			
		\end{block}
		
	\end{frame}
	
	
	\begin{frame}\frametitle{Optimal ventilation for humans}
		$$ \underset{V_T,f_b}{\min} \, \,  \mathcal{P}_{\rm e}(V_T,f_b) + \mathcal{P}_{\rm a}(V_T,f_b) \quad \text{s.t.} \, \, \dot{V}_{O_2}(V_T,f_b) = \dot{V}_{O_2}^{\rm{obs}}$$
		
		\begin{figure}
			\includegraphics[scale=0.38]{images/fred_opti_ventil}
			%\caption{Total power expenditure during ventilation for different intensitues of exercises}
		\end{figure}
	\end{frame}
	% section the_lung_as_a_model_organ_for_optimization_under_constraints (end)
	
	\section{Allometric scaling laws} % (fold)
	\label{sec:allometric_scaling_laws}
	
	\begin{frame}
		\begin{center}
			\Huge{ \textcolor{EPCPpurpleDark}{Allometric scaling laws}}
		\end{center}
	\end{frame}
	
	
	\subsection{Concept of allometry}
	\begin{frame}\frametitle{Concept of allometry}
	
	\begin{table}[]
	\begin{tabular}{cc}
	Raw ecological data & Log-Log plot\\
	 &  \\
	%\mbox{ } & ${\scriptstyle Y = Y_0 \, M^{\alpha}}$ \\
	\includegraphics[height=0.45\paperheight]{images/ORG_PlotCart.pdf} & \includegraphics[height=0.45\paperheight]{images/ORG_PlotLog.pdf} \\
	\end{tabular}
	\end{table}

	$$ t_s = 10.1 \, M^{-0.103}$$ 
	
	\end{frame}
	
	\begin{frame}\frametitle{History of allometry}

		\begin{itemize}
			\item 1897: Eugène Dubois described the relation between the brain's mass and the body's mass in mammals
			 $$b = c \, m^r $$
			\item 1907: Lapicque transformed Dubois' relation in a log-log dependency
			\item 1917: D'Arcy Thompson adopted the thesis that the living systems are submitted to the physical laws of nature 
			\item 1936: Huxley and Tessier agreed for the terminology of allometry and the associated law $$ y = bx^\alpha $$
		\end{itemize}
	
	%\begin{table}[]
	%\begin{tabular}{cc}
	%\multicolumn{2}{c}{\includegraphics[height=0.20\paperheight]{images/huxley.png}} \\
	%\mbox{ } & \mbox{ } \\
	%\includegraphics[height=0.20\paperheight]{images/darcy.png} & \includegraphics[height=0.20\paperheight]{images/turing.png} \\
	%\end{tabular}
	%\end{table}
	
	\end{frame}
	
	
	\begin{frame}\frametitle{Mechanistic approach }

		\begin{block}{WBE -- Hypotheses (1997)}
			\begin{enumerate}
				\item Transport of nutrients i.e., oxygen in a fractal-like branching tree
		
				%\item Metabolic rate $\propto$ flow of nutrient-carrier i.e., blood; independent of body size
		
				\item Fluid carrier incompressible
		
				\item Total volume of the fluid proportional to body size
		
				\item Size of the terminal units i.e., capillaries invariant or mass independent 
				\smallskip	
				\begin{figure}
					\centering \includegraphics[height=0.35\paperheight]{images/wbe_reduced_pdf.pdf}
				\end{figure} 
				
			\end{enumerate}
		\end{block}
	
	
	\end{frame}
	
	\begin{frame}\frametitle{Mechanistic approach}
	
	\begin{block}{WBE -- Model \& Results}
		\begin{itemize}
			\item General metabolic allometry follows a $ \propto$ $M^{\frac34}$ relation
	
			\item Data-based allometric relations are retrieved from the model 
				\end{itemize}
			
			\bigskip
			
			\begin{table}
			\scriptsize
			\begin{tabular}{cccccc}
			\multicolumn{3}{c}{Cardiovascular}              & \multicolumn{3}{c}{Respiratory}                        \\ \hline
			Variable         & \multicolumn{2}{c}{Exponent} & Variable                & \multicolumn{2}{c}{Exponent} \\ \cline{2-3} \cline{5-6} 
											 & Observed     & Predicted      &                         & Observed    & Predicted      \\ \hline
			Aorta radius     & 0.36         & 3/8 = 0.375    & Trachea radius          & 0.39        & 3/8 = 0.375    \\
			Blood volume     & 1.00         & 1.00           & Lung volume             & 1.05        & 1.00           \\
			Circulation time & 0.25         & 1/4 = 0.25     & Respiratory frequency   & -0.26       & -1/4 = -0.25   \\
			Metabolic rate   & 0.75         & 3/4 = 0.75     & Air velocity in trachea & 0.02        & 0              \\  
			\end{tabular}
			\end{table}
	
	
	\end{block}
	
	\end{frame}
	
	
	\subsection{The respiratory system}
	
	\begin{frame}\frametitle{Allometric laws in the respiratory system}
		\begin{itemize}
			\item Mammals share morphological and functional properties dependent on the mass of the animal with allometric scaling laws
			\item Morphological differences amongst mammals affect the control of ventilation
		\end{itemize}
		
		\begin{figure}
			\includegraphics[scale=0.33]{images/AllometricVO2max}
		\end{figure}
	\end{frame}
	
	\begin{frame}\frametitle{Adaptation of the oxygen transport model}
		\begin{block}{Shared characteristics}
			\begin{itemize}
				\item Tree-like structure with bifurcating branches
				\item Decomposition into two parts: bronchial tree and acini
			\end{itemize}
		\end{block}
		
		\begin{block}{Adaptation of morphological parameters }
			\begin{itemize}
				\item Tracheal radius and length
				\item Radius and length of alveolar ducts
				\item Exchange surface
			\end{itemize}
		\end{block}
		
		\begin{block}{Oxygen transport}
			\begin{itemize}
				\item Convection-diffusion-reaction equation
				\item Exchange $\beta$ coefficient dependent on the mass of the mammal
			\end{itemize}
		\end{block}
	\end{frame}
	
	\begin{frame}\frametitle{Optimal ventilation for mammals}
		$$ \underset{V_T,f_b}{\min} \, \,  \mathcal{P}_{\rm e}(V_T,f_b) + \mathcal{P}_{\rm a}(V_T,f_b) \quad \text{s.t.} \, \, \dot{V}_{O_2}(V_T,f_b) = \dot{V}_{O_2}^{\rm{obs}}$$
		
		\begin{figure}[htpb]
			\centering
			\subfloat[Frequency]{\includegraphics[scale=0.3]{images/OptiMammalfreq.png}}
			\subfloat[Tidal Volume]{\includegraphics[scale=0.3]{images/OptiMammalVol.png}}
			%\caption{Predicted ventilation frequency (s$^{-1}$ -- \textit{left}) and tidal volume (L -- \textit{right}) as a function of the mammal mass (kg -- log-log scale) at different metabolic regimes. BMR: Basal Metabolic Rate, FMR: Field Metabolic Rate, MMR: Maximal Metabolic Rate.}
			\label{fig:OptiMammal}
		\end{figure}
	\end{frame}
	
	\begin{frame}\frametitle{Allometric laws for ventilation}
		
		Allometric law:
		$$ Y = Y_0 M^\alpha $$
		
		\begin{table}
		\begin{tabular}{ccccc}
		& $f_b$ (pred) & $f_b$ (obs) & $V_T$ (pred) & $V_T$ (obs) \\
		\hline
		BMR & -0.29 & -0.26 & 1.05 & 1.04 \\
		FMR & -0.32 & N.D & 0.98 & N.D. \\
		MMR & -0.15 & -0.14 & 1.04 & N.D. \\
		\hline
		\end{tabular}
		\caption{Predicted and observed exponents $\alpha$ for the allometric scaling laws of breathing frequency $f_b$ and tidal volume $V_T$ at three different metabolic regimes.}
		\end{table}
	\end{frame}
	
	% \begin{frame}\frametitle{Convection diffusion transition}
	% 	\begin{figure}
	% 	\centering
	% 	\includegraphics[width=0.95\textwidth]{images/peclet_crop_bm.png}
	% 	\label{fig:screening}
	% 	\end{figure}
	% \end{frame}
	
	% section allometric_scaling_laws (end)
	
	\section{Conclusion} % (fold)
	\label{sec:conclusion}
	
	\begin{frame}
		\begin{center}
			\Huge{ \textcolor{EPCPpurpleDark}{Conclusion}}
		\end{center}
	\end{frame}
	
	\begin{frame}\frametitle{Conclusion}
		\begin{itemize}
			\item Principles of economy applied on larger living structures
			\item Constraints guide the development and the functioning of mammalian lung
			\item Allometric laws allow a deep understanding of the mechanisms of differential growth
		\end{itemize}
	\end{frame}
	
	% section conclusion (end)
	
	\end{document}
	

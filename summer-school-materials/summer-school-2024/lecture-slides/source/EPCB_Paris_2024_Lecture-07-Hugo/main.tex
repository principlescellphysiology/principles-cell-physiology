\documentclass[aspectratio=169]{beamer}

\usepackage{ifluatex}
\usepackage{bm}
\usepackage{mathtools}
\usepackage{tikz-cd}
\usetikzlibrary{shapes.geometric, arrows}
\usepackage{blkarray}
\usepackage{changepage}
\usepackage{amsmath}
\usepackage{ulem}
\usepackage{soul}

\definecolor{gold}{rgb}{1.0, 0.80, 0.0}
\definecolor{olive}{rgb}{0.13, 0.55, 0.13} 
%
\title{~\\
Growth Balance Analysis}
\author{Hugo Dourado \\
~\\
\footnotesize Institute for Computer Science and Department of Biology\\
Heinrich-Heine Universit{\"a}t, D{\"u}sseldorf\\
~\\
July 9, 2024}
\date{July 9, 2024}

%
\definecolor{piggyred}{RGB}{255,212,213}
\definecolor{EPCPpurple}{RGB}{105,88,153}
\definecolor{EPCPpurpleMedium}{RGB}{159,139,213}
\definecolor{EPCPpurpleDark}{RGB}{31,0,81}
\definecolor{EPCPocher}{RGB}{254,244,254}
\definecolor{INRIAgreen}{RGB}{0,163,166}

\setbeamercolor{EPCP}{fg=white,bg=EPCPpurpleMedium}
\usecolortheme[named=EPCPpurple]{structure}
%
\setbeamerfont{title}{size=\huge}%
\setbeamercolor{title}{fg=EPCPpurpleDark}
%
\setbeamerfont{author}{size=\large}%
\setbeamercolor{author}{fg=EPCPpurpleDark}
%
\setbeamercolor{frametitle}{fg=black}%
\setbeamercolor{footline}{fg=white,bg=EPCPpurpleMedium}
\setbeamercolor{itemize item}{fg=EPCPpurple}
\setbeamercolor{enumerate item}{fg=EPCPpurpleDark}
%
\setbeamercolor{block title}{fg=white,bg=EPCPpurpleDark}%
\setbeamercolor{block body}{fg=black,bg=EPCPocher!5}%
\setbeamercolor{block title example}{fg=EPCPocher!5,bg=INRIAgreen}%
\setbeamercolor{block body example}{fg=EPCPpurple,bg=INRIAgreen!5}%
\setbeamercolor{block title alerted}{fg=red!50!black,bg=piggyred}%
\setbeamercolor{block body alerted}{fg=black,bg=white}%
%
\setbeamercolor{frametitle}{fg=black}%
%
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamertemplate{navigation symbols}{}

%
\usefonttheme{professionalfonts}
\ifluatex
\usepackage{fontspec}
\setmainfont{garamond}
\usepackage{unicode-math}
\setmathfont{garamond}
\else
\usepackage[utf8]{inputenc}
\fi
%

\usepackage[version=4]{mhchem}
\usepackage[square,numbers]{natbib}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{subcaption}
\usepackage{graphicx}

%\makeatletter
\mode<all>{
\ifpdf
\pgfdeclareimage[height=0.2\paperheight]{piggyBac}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=2.3ex]{piggyBacSmall}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=0.05\paperheight]{INRIA}{images/inria-logo.jpg}%
\pgfdeclareimage[height=0.05\paperheight]{MathNum}{images/mathnum-purple.jpg}%
\pgfdeclareimage[height=0.08\paperheight]{Eurip}{images/eurip-purple.jpg}%
\pgfdeclareimage[height=0.11\paperheight]{LPI}{images/LPI-purple.png}%
%
\else
% TODO? eps version
\fi

\setbeamertemplate{title page}{
\begin{beamercolorbox}[wd=\paperwidth]{EPCP}
\parbox[b]{.12\paperwidth}{\hspace*{.5em}\\[-1ex]\mbox{}}
\hfill
\begin{minipage}[b][0.2\paperheight]{.6\paperwidth}{%
		\begin{center}\LARGE
			Economic Principles in Cell Biology\\[3mm]
			{\footnotesize \color{EPCPpurpleDark}Paris, July 8--11, 2024\\[-2ex]\mbox{}}
		\end{center}
	}
\end{minipage}
\hfill
\pgfuseimage{piggyBac}
\parbox[b]{.04\paperwidth}{\hspace*{.5em}\\[-1ex]\mbox{}}
\end{beamercolorbox}

\vskip0pt plus 1filll
\begin{center}
{\usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle}\\
\vspace{1cm}
{\usebeamerfont{author}\usebeamercolor[fg]{author}\insertauthor}
\end{center}

\mbox{}
\vskip0pt plus 1filll
\begin{beamercolorbox}[wd=\paperwidth]{title}
  \hspace{1cm}\pgfuseimage{INRIA}\hspace{.5cm}\pgfuseimage{MathNum}
  \mbox{}\hfill
  \pgfuseimage{Eurip}\hspace{.5cm}\pgfuseimage{LPI}\hspace{1cm}\mbox{}\hfill\\[4mm]
\end{beamercolorbox}
}% end title page
%
\setbeamertemplate{footline}{
	\begin{beamercolorbox}[wd=\paperwidth]{footline}
		\hfill
		{\mbox{}\raise1.3ex\hbox{\insertshorttitle\hspace*{1em}\insertframenumber\slash\inserttotalframenumber\hspace*{.2em}}}
		\pgfuseimage{piggyBacSmall}
	\end{beamercolorbox}
}% end footline
}% end mode<all>

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START PRESENTATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
	
{\setbeamercolor{background canvas}{bg=EPCPocher}
\begin{frame}[plain]
	\titlepage
\end{frame}
%\maketitle
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Why yet another ``balance analysis''?}

\textbf{Growth Balance Analysis (GBA):} {\color{INRIAgreen}simplified} framework for {\color{red}nonlinear} {\color{blue}self-replicating} cell models at {\color{EPCPpurple}balanced growth}\footnote{\tiny Dourado \& Lercher, An analytical theory of balanced cellular growth, \textit{Nature Communications} 2020.}.

\bigskip

\begin{itemize} \setlength\itemsep{1em}
    \item \textbf{\color{red}Nonlinear:} includes nonlinear kinetic rate laws.

    \item \textbf{\color{blue}Self-replicating:} metabolism + protein synthesis and dilution of \underline{\textbf{all}} components.

    \item \textbf{\color{EPCPpurple}Balanced growth:} constant (external and internal) concentrations in time.
\end{itemize}

\bigskip

\textbf{A framework, not a model:} find common properties to all possible models.

\bigskip

{\color{INRIAgreen}\textbf{Mathematical simplification:}} allows analytical study to find fundamental principles.

\bigskip
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Mathematical simplification: the least number of variables and equations}

\bigskip

\textbf{Not important for linear problems, but critical for nonlinear problems!}

\bigskip

\textbf{Example:} Simple pendulum
\begin{columns}
    \begin{column}{0.3\textwidth} 
        \begin{figure}
            \centering
            \includegraphics[width=0.5\linewidth]{pendulum.png}
        \end{figure}
    \end{column}
    \begin{column}{0.59\textwidth} 
        Angle $\theta$ (``generalized coordinate'') completely
        
        determines the system state, no need of x,y,z. 
    \end{column}
\end{columns}

\bigskip

\textbf{Why looking for simplest formulation?}
\begin{itemize}
    \item Easier numerical calculations.

    \item Independent variables are preferable for analytical methods.

    \item Deeper understanding of the problem.

    \item Most ``elegant'' solution.
\end{itemize}

    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Balanced growth (or steady growth)}

\textbf{For a steady-state environment defined by ``external'' concentrations $\mathbf{x}$:}

\bigskip

\begin{itemize}\setlength\itemsep{1em}
    \item Steady-state growth rate $\mu$ (1/h), direct measure of fitness.

    \item Steady-state internal concentrations $\mathbf{c}$ (g/L) of reactants (substrates, products)
    \begin{equation*}
    c_i  = \frac{\text{abundance of ``i'' (g/cell)}}{\text{volume (L/cell)}} = \text{constant}
\end{equation*}

    
\end{itemize}

\bigskip

\textbf{Mass concentrations (not abundances) better describe cell states:} i) constant, ii) reaction kinetics depend on concentrations, iii) relate to cell density (g/L).

\bigskip

\textbf{Matching units for fluxes $\mathbf{v}$:} mass per volume per time (g L$^{-1}$ h$^{-1}$).

\bigskip

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{``Self-replicator'' models: Molenaar et al. \textit{Mol Sys Biol} 2009}

\bigskip

\textbf{``Self-replicating'':} nonlinear kinetics for transport + metabolism + protein synthesis, with dilution by growth of \underline{\textbf{all}} components (no biomass input, now it is an output).

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{cell.png}
\end{figure}


\textbf{Optimal state:} maximize $\mu$ limited by mass conservation, kinetics and total protein.  

\bigskip

\textbf{Self-replicating models must be nonlinear:} saturation/dilution trade-off.
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Density constraint(s)}

\textbf{Linear models:} fixed density of  ``biomass'' (diffuse concept), total protein $c_{\rm p}$.

\bigskip

\textbf{Self-replicator models [a.k.a. Molenaar models]:} fixed total protein $c_{\rm p}$.

\bigskip

\textbf{GBA:} fixed cell density $\rho$ (g/L) including \textbf{all} components (indicated by experiments\footnote{\tiny Baldwin et al.\textit{Archives of Microbiology} 1995, Kubitschek et al. \textit{Journal of bacteriology} 1983,  Cayley et al. \textit{Journal of Molecular Biology} 1991})
\begin{equation*}
    \rho = c_{\rm p} + \sum_m c_m
\end{equation*}
where $m$ are all ``non-protein'' components, and assumed unique protein composition\footnote{\tiny Dourado et al. \textit{PLOS Comp Bio} 2023.}.

\bigskip

\textbf{GBA units:} mass concentration (g/L) is the most convenient unit. To match units, we normalize the stoichiometric matrix $\mathbf{S}$ with the molecular weights $\mathbf{w}$ (g/mol)
\begin{equation*}
    {\mathbf{S}^{\rm total}} \xrightarrow[\text{columns by} \, \mathbf{w}]{\text{multiply}}  {\text{diag}(\mathbf{w}) \, \mathbf{S}^{\rm total}} \xrightarrow[\text{columns}]{\text{normalize}} \mathbf{M}^{\rm total} \xrightarrow[\text{external rows}]{\text{exclude}}  \mathbf{M} 
\end{equation*}
    

\end{frame}

\begin{frame}{The ``mass fraction matrix'' $\mathbf{M}$}
   
\begin{columns}
\begin{column}{0.5\textwidth}   
    \centering
\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{M.png}
\end{figure}
\end{column}
\begin{column}{0.5\textwidth}   
    \centering
\begin{align*}   
    &~~ j  =\text{reactions (\textcolor{olive}{transport s}, \textcolor{blue}{enzymatic e}, \textcolor{red}{ribosome r})}   \\ 
    &~~ i  =\text{internal reactants (\textcolor{gray}{``metabolites'' $m$}, \textcolor{gold}{protein \rm p}) }  \\ 
    &   \\
    & ~~~~\text{(upper index = rows, internal reactants)}\\
    & ~~~~\text{(lower index = columns, reactions)}
\end{align*}
\end{column}
\end{columns}

\textbf{``Mass accretion'' vector $\mathbf{a}$:} the sum of each column
\begin{equation*} \label{eq:gamma}
    a_j \coloneqq \sum_i M^i_j ~~~ = 
    \begin{cases}
    \neq 0 ~~~ , j=s\\
    = 0 ~~~    , j=e\\
    = 0 ~~~    , j=\rm r\\
    \end{cases} ~~~~ \Leftrightarrow ~~~~ \mathbf{w}^\top \mathbf{S}^{\rm total} = \mathbf{0}^\top
\end{equation*}

    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Example of a GBA model: ``model A''}
\footnotesize
\begin{exampleblock}{Three Michaelis-Menten reactions in series}
\textbf{A) Model scheme}
\begin{figure}
\centering
    \includegraphics[width=0.5\linewidth]{A.png}
\end{figure}
 \textbf{B) Model parameters}
\begin{equation*}
\mathbf{M} = \begin{blockarray}{cccc}
~~\textcolor{olive}{1} & ~~\textcolor{blue}{2} & ~~\textcolor{red}{3} \\
\begin{block}{[ccc]c}
  ~1 &  -1 & ~~0 & \textcolor{gray}{1} \\
  ~0 & ~~1 &  -1 & \textcolor{gray}{2} \\
  ~0 & ~~0 & ~~1 & \textcolor{gold}{3} \\
\end{block}
\end{blockarray} ~~, ~ \mathbf{K} = \begin{blockarray}{cccc}
\textcolor{olive}{1} & \textcolor{blue}{2} & \textcolor{red}{3} \\
\begin{block}{[ccc]c}
        1  &0  &0 & \textcolor{brown}{1}\\
        0  &22 &0 & \textcolor{gray}{1}\\
        0  &0  &40 & \textcolor{gray}{2} \\
        0  &0  &0 & \textcolor{gold}{3}\\
\end{block}
\end{blockarray} ~~, ~  \mathbf{k}_{\rm cat} = \begin{blockarray}{ccc}
\textcolor{olive}{1} & \textcolor{blue}{2} & \textcolor{red}{3} \\
\begin{block}{[ccc]}
  6 & 6 & 5 \\
\end{block}
\end{blockarray} ~~,~ 
    \rho = 340 ~ g/L
\end{equation*}
    
\end{exampleblock}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Example of a GBA model: ``model B''}

\begin{exampleblock}{Model with replication, transcription, Michaelis-Menten kinetics with activation}
\vspace{-0.5cm}
\begin{figure}
\tiny
\begin{minipage}{0.48\textwidth}
\includegraphics[width=0.9\textwidth]{modelB.png}
\end{minipage}
\begin{minipage}{0.48\textwidth}
\begin{equation*}
\mathbf{M} = \begin{blockarray}{cccccc}
~\textcolor{olive}{s} & ~~\textcolor{blue}{Met} & ~~\textcolor{blue}{DNAp} & ~~\textcolor{blue}{RNAp} & \textcolor{red}{r} \\
\begin{block}{[ccccc]c}
  ~1 &  -1   & ~~0 & ~~0 & ~~0 ~~~ & \textcolor{gray}{C} \\
  ~0 & ~~0.2 & ~~0 & ~~0 & -0.3 ~~~ & \textcolor{gray}{ATP} \\
  ~0 & ~~0.3 &  -1 &  -1 & ~~0 ~~~ & \textcolor{gray}{NT} \\
  ~0 & ~~0.5 & ~~0 & ~~0 &  -0.7 ~~~ & \textcolor{gray}{A} \\
  ~0 & ~~0   & ~~1 & ~~0 & ~~0 ~~~ & \textcolor{gray}{DNA} \\   
  ~0 & ~~0   & ~~0 & ~~1 & ~~0 ~~~ & \textcolor{gray}{RNA} \\
  ~0 & ~~0   & ~~0 & ~~0 & ~~1 ~~~ & \textcolor{gold}{p} \\
\end{block}
\end{blockarray}
\end{equation*}
\begin{equation*}
\mathbf{K} = \begin{blockarray}{cccccc}
~\textcolor{olive}{s} & \textcolor{blue}{Met} & \textcolor{blue}{DNAp} & \textcolor{blue}{RNAp} & \textcolor{red}{r} ~~ \\
\begin{block}{[ccccc]c}
  ~1 & 0  & 0 & 0 & 0 ~~~ & \textcolor{brown}{C_{ex}} \\
  ~0 & 11 & 0 & 0 & 0 ~~~ & \textcolor{gray}{C} \\
  ~0 & 0  & 0 & 0 & 12 ~~~ & \textcolor{gray}{ATP} \\
  ~0 & 0  & 10& 10& 0 ~~~ & \textcolor{gray}{NT} \\
  ~0 & 0  & 0 & 0 & 25 ~~~ & \textcolor{gray}{A} \\
\end{block}
\end{blockarray}
\end{equation*}
\begin{equation*}
\mathbf{A} = \begin{blockarray}{cccccc}
~\textcolor{olive}{s} & \textcolor{blue}{Met} & \textcolor{blue}{DNAp} & \textcolor{blue}{RNAp} & \textcolor{red}{r} \\
\begin{block}{[ccccc]c}
  ~0 & 0 & 3 & 3 & ~~0 ~~~ & \textcolor{gray}{DNA} \\   
  ~0 & 0 & 0 & 0 & ~~5 ~~~ & \textcolor{gray}{RNA} \\
\end{block}
\end{blockarray}
\end{equation*}
\begin{equation*}
\mathbf{k}_{\rm cat} = \begin{blockarray}{ccccc}
\textcolor{olive}{s} & \textcolor{blue}{Met} & \textcolor{blue}{DNAp} & \textcolor{blue}{RNAp} & \textcolor{red}{r} \\
\begin{block}{[ccccc]}
  13 & 24 & 7 & 17 & ~~4 ~~  \\ 
\end{block}
\end{blockarray}  ~,~ 
    \rho = 340  
\end{equation*}
\end{minipage}
\end{figure}
\end{exampleblock}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The general GBA problem}

\textbf{For some given model $(\mathbf{M}, \bm{\tau}, \rho)$ and environment $\mathbf{x}$:}
\begin{align*}
    & \underset{\mathbf{v} \, \in \, \mathbb{R}^{\rm r}, \mathbf{c} \, \in \, \mathbb{R}^{\rm p}}{\text{maximize}} && \mu ~~~ &&\text{(Maximize growth rate)}\\
    & \text{subject to:} \\
    &&& \mathbf{M} \, \mathbf{v} = \mu \, \mathbf{c}  ~~~ &&\text{(Flux balance)} \\
    &&& c_{\rm p} = \mathbf{v}^\top \bm{\tau}(\mathbf{c},\mathbf{x})    ~~~ &&\text{(Reaction kinetics and protein sum)} \\
    &&& \rho = \sum \mathbf{c} ~~~ &&\text{(Constant cell density)} \\
    &&& \mathbf{v} \odot \bm{\tau}(\mathbf{c},\mathbf{x}) \geq \mathbf{0} ~~~ &&\text{(non-negative protein concentrations)} \\
    &&& \mathbf{c} \geq \mathbf{0} ~~~ &&\text{(non-negative reactant concentrations)}
\end{align*}

where $\odot$ indicates the element-wise multiplication.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Approximated problem: maximization of $\mu$ with global mass conservation}

\bigskip

\textbf{The net mass uptake:} enforced by the sum all equations in $\mathbf{M} \, \mathbf{v} = \mu \, \mathbf{c}$

\begin{equation*}
    v_{\rm uptake}= v_{\rm in} - v_{\rm out} = \sum_{i,j} M^i_j \, v^j = \mu \sum_i c_i = \mu \, \rho ~~~,
\end{equation*}

thus, 
\begin{equation*}
    \mu(\mathbf{v},\mathbf{c}) = \frac{\sum_{i,j} M^i_j \, v^j}{ \rho(\mathbf{c})} ~~~.
\end{equation*}

\textbf{For any given $\mathbf{v}$:} 

\bigskip

\begin{center}
    maximal $\mu(\mathbf{c})$ $\Leftrightarrow$ minimal $\rho(\mathbf{c}) =  c_{\rm p} + \sum_m c_m$
\end{center}

\bigskip

\textbf{Accounting for kinetics and protein sum:}

\begin{equation*}
    \rho (\mathbf{c})  = \sum_j v_j \, \tau_j(\mathbf{c})  + \sum_m c^m
\end{equation*}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Approximated problem: optimal density $\rho$}

\bigskip

\textbf{The optimal state must satisfy}
\begin{equation*}
    \boxed{{\color{olive}\frac{\partial \rho }{\partial c^m}} =  {\color{blue} \sum_j v_j \, \frac{\partial \tau_j}{\partial c^m}}  +  {\color{red}1} = 0 ~~~ \forall ~~ m}
\end{equation*}


\begin{block}{Economics analogy: marginal cost from kinetic benefit and density cost}
	{\color{olive}marginal cost} =  {\color{blue}marginal kinetic benefit} +  {\color{red}marginal density cost} (= 0 if optimal)
\end{block}

\begin{alertblock}{Note: the kinetic benefit is the protein saved due to increased saturation ($< 0$)}
\begin{equation*}
    {\color{blue}\sum_j v_j \, \frac{\partial \tau_j}{\partial c^m}}  = {\color{blue} \sum_j \left(\frac{\partial p_j}{\partial c^m}\right)_{\mathbf{v} = const.}} 
\end{equation*}
\end{alertblock}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Optimal density: validation}

\bigskip

\textbf{Simplest case:} Michaelis-Menten kinetics and a 1-to-1 reaction-substrate relationship
\begin{equation} \label{eq:pc}
    \boxed{p_j = c^m \left(1 + \frac{c^m}{K^m_j} \right)}
\end{equation}

\textit{E. coli} enzymes and substrates are close to \emph{this} optimality\footnote{\tiny Dourado et al. On the optimality of the enzyme–substrate relationship in bacteria, \textit{PLOS Biology} 2021.}
\begin{figure}
    \centering
    \includegraphics[width=0.32\linewidth]{metabolite.png}
\end{figure}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The GBA problem with no alternative pathways: full column rank $\mathbf{M}$ }

\textbf{For some given model $(\mathbf{M}, \bm{\tau}, \rho)$ and environment $\mathbf{x}$:}
\begin{align*}
    & \underset{\mathbf{v} \, \in \, \mathbb{R}^{\rm r},\mathbf{c} \, \in \, \mathbb{R}^{\rm p}}{\text{maximize}} && \mu ~~~ &&\text{(Maximize growth rate)}\\
    & \text{subject to:} \\
    &&& \mathbf{M} \, \mathbf{v} = \mu \, \mathbf{c}  ~~~ &&\text{(Flux balance)} \\
    &&& c_{\rm p} = \mathbf{v}^\top \bm{\tau}(\mathbf{c},\mathbf{x})    ~~~ &&\text{(Reaction kinetics and protein sum)} \\
    &&& \rho = \sum \mathbf{c} ~~~ &&\text{(Constant cell density)}\\
    &&& \text{\sout{$\mathbf{v} \odot \bm{\tau}(\mathbf{c},\mathbf{x}) \geq \mathbf{0}$}} ~~~ && \text{\st{(non-negative protein concentrations)}} \\
    &&& \mathbf{c} \geq \mathbf{0} ~~~ &&\text{(non-negative reactant concentrations)}
\end{align*}

\bigskip

\textbf{Simplest case:} there is a inverse $\mathbf{W} = \mathbf{M}^{-1}$, so:
\begin{equation*}
     \mathbf{v} = \mu \, \mathbf{W} \, \mathbf{c} 
\end{equation*}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The GBA problem with no alternative pathways: formulation on $\mathbf{c}$}


\begin{block}{For invertible $\mathbf{M}$: formulation on $\mathbf{c}$ in few steps}
	
\textbf{Substituting}  $\mathbf{v} = \mu \, \mathbf{W} \, \mathbf{c}$ ~ into ~ $c_{\rm p} = \mathbf{v}^\top \bm{\tau}(\mathbf{c},\mathbf{x})$
\begin{equation*} 
    c_{\rm p} = \mu \left(\mathbf{W} \, \mathbf{c}\right)^\top \bm{\tau}(\mathbf{c},\mathbf{x}) ~~~.
\end{equation*}

\textbf{Solving for $\mu$}: we get the objective function $\mu(\mathbf{c},\mathbf{x})$
\begin{equation*}
    \boxed{\mu(\mathbf{c},\mathbf{x}) = \frac{c_{\rm p}}{\displaystyle \left(\mathbf{W} \, \mathbf{c}\right)^\top \bm{\tau}(\mathbf{c},\mathbf{x})}} ~~~.
\end{equation*}

\textbf{The only constraint left:}
\begin{equation*}
    \boxed{\rho = \sum \mathbf{c}} ~~~.
\end{equation*}

\bigskip

\textbf{The problem is now completely reformulated on $\mathbf{c}$ as independent* variables} 
\end{block}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The GBA problem with no alternative pathways: analytical ``solution''}

\textbf{Reformulated problem:} for some given model $(\mathbf{M}, \bm{\tau}, \rho)$ and environment $\mathbf{x}$
\begin{equation*} \label{eq:optimization_c}
    \begin{aligned}
    & \underset{\mathbf{c} \, \in \, \mathbb{R}^{\rm p}_+}{\text{maximize}} && \mu(\mathbf{c},\mathbf{x}) = \frac{c_{\rm p}}{(\mathbf{W} \, \mathbf{c})^\top \bm{\tau}(\mathbf{c},\mathbf{x}) } \\
    & \text{subject to:} \\
    &&& \sum \mathbf{c} = \rho  ~~~.
    \end{aligned}
\end{equation*} 

\textbf{Analytical conditions for optimal states:} using Lagrange multipliers, we find 
\begin{equation} \label{eq:balance_c}
    \boxed{\mu \, (\mathbf{W} \, \mathbf{c})^\top \frac{\partial \bm{\tau}}{\partial c_m} + \mu \, \bm{\tau}^\top \left( \mathbf{W}_m - \mathbf{W}_{\rm p}  \right) + 1 = 0 ~~~ \forall ~ m}
\end{equation}

\textbf{With $\sum \mathbf{c} = \rho$ :} $\rm p - 1$ algebraic equations on $\rm p - 1$ variables (solvable).

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The GBA problem with no alternative pathways: economics}

\bigskip

\textbf{Substituting $\mathbf{v} = \mu \, \mathbf{W} \, \mathbf{c}$ into the solution (\ref{eq:balance_c})}
\begin{equation*} \label{eq:balance_cv}
    {\color{olive}\frac{c_{\rm p}}{\mu} \frac{\partial \mu}{\partial c^m}} =
    {\color{blue}- \, \sum_j v_j \, \frac{\partial \tau^j}{\partial c^m}} \,  {\color{EPCPpurple} + \, \mu  \sum_j \tau_j \, \left(W^j_{\rm p} - W^j_m \right)}  \, {\color{red} - \, 1} = 0 ~~~ \forall ~ m
\end{equation*}
 \begin{block}{\footnotesize Economics analogy: new ``structural'' marginal benefit, the proportional decrease in protein allocation}
\centering
\footnotesize
	{\color{olive} (marginal) value} = {\color{blue}kinetic benefit} + {\color{EPCPpurple} protein allocation benefit} +  {\color{red} density cost} (= 0 if optimal)
\end{block}

\begin{alertblock}{\footnotesize Because of $\mathbf{v} = \mu \, \mathbf{W} \, \mathbf{c}$ and $c_{\rm p} = \rho - \sum_m c^m$: increasing $c^m$ also causes a protein allocation decrease 
}
\begin{equation*}
   {\color{EPCPpurple}- \sum_j \left(\frac{\partial p^j}{\partial c^m}\right)_{\bm{\tau}, \, \mu = const.} = - \sum_j \tau_j \frac{\partial v^j}{\partial c^m} = {\color{EPCPpurple}\underbrace{\mu  \sum_j \tau_j \, W^j_{\rm p}}_\text{protein production} - \underbrace{\mu  \sum_j \tau_j \, W^j_m}_\text{metabolite production} }}
\end{equation*}
\footnotesize This contribution is typically very low\footnote{\tiny Dourado, Quantitative principles of optimal cellular resource allocation, \textit{PhD Thesis} 2020.}, around 0.03 (explains why predictions of eq.(\ref{eq:pc}) are good).
\end{alertblock}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Equations for balance growth states: model A}
\scriptsize{
\begin{figure}
\raggedright{\textbf{1) Original problem: Implicit constraints on $\mu$, involving $v_1,v_2,v_3, c_1,c_2,c_3, x_1$ (6 variables, 5 equations)}}
\begin{align*}
    v_1 - v_2 &= \mu \, c_1  \\
    v_2 - v_3 &= \mu \, c_2  &&\textbf{(mass conservation)}  \\
          v_3 &= \mu \, c_3 \\
    \frac{v_1}{6} \left(1 + \frac{1}{x_1} \right) + \frac{v_2}{6} \left(1 + \frac{22}{c_1} \right) + \frac{v_3}{5} \left(1 + \frac{40}{c_2} \right) &= c_3  &&\textbf{(kinetics and protein sum)}  \\
    c_1 + c_2 + c_3 &= 340 &&\textbf{(constant cell density)}
\end{align*}
 
\textbf{2) GBA: Explicit constraint on $\mu(c_1,c_2, x_1)$ (using $c_3 = 340 - c_1 - c_2$)}

\begin{equation*}
    \mu (c_1,c_2, x_1) = \frac{340 - c_1 - c_2}{\displaystyle \frac{1}{6} \left(1 + \frac{1}{x_1} \right) + \frac{340 - c_1}{6 \cdot 340} \left(1 + \frac{22}{c_1} \right)  + \displaystyle \frac{340 - c_1 - c_2}{5 \cdot 340} \left(1 + \frac{40}{c_2} \right) } ~~~ \textbf{(constrained growth rate)}
\end{equation*} % f_1 = \frac{1 - u_h \, q^h }{a_1} = 1 , qdot_1 = 0

\bigskip

\textbf{3) Analytical conditions for optimal balanced growth state (system of algebraic equations)}
\begin{align*}
    {\color{blue} \mu \frac{22}{6} \frac{(340 - c_1)}{(c_1)^2}}  {\color{EPCPpurple} \, + \, \mu \left[ \frac{1}{6} \left(1 + \frac{22}{c_1} \right) + \frac{1}{5} \left(1 + \frac{40}{c_2} \right)   \right]}  {\color{red}\, - \, 1} &= 0 ~~~~ (m = 1)\\
    {\color{blue} \mu \, \frac{40 \, (340 - c_1 - c_2)}{5 \, (c_2)^2}} \, +  {\color{EPCPpurple}\mu \left[ \frac{1}{5} \left(1 + \frac{40}{c_2} \right)   \right]}  {\color{red}\, - \, 1} &= 0 ~~~~ (m = 2)\\
\end{align*}
\end{figure}
}
    
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Numerical solutions for different external concentrations $x$: model A}

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{simulationA.png}
\end{figure}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Comparison to data: \textit{E. coli} and yeast ribosomal protein $\phi_{\rm r}$ vs. $\mu$}

\textit{in vivo} data close to the predicted optimality\footnote{\tiny Dourado \& Lercher, An analytical theory of balanced cellular growth, \textit{Nature Communications} 2020.} ({\color{red}red lines, no fitting}).

\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{rib.png}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{``Growth Control Analysis'': holistic view of the growing cell}

\bigskip

\textbf{Metabolic Control Analysis (MCA):} perturbations on metabolism (open system).

\bigskip

\textbf{Growth Control Analysis (GCA):} perturbations self-replicating system (closed system), all is connected $\Rightarrow$ analytical expressions\footnote{\tiny Dourado \& Lercher, \textit{Nature Communications} 2020, $^7$ Cayley et. al, \textit{Biophys. J} 2000}.  

\bigskip

\begin{itemize} \setlength\itemsep{1em}
    \item Growth Control Coefficients $\Gamma$: change in $\mu$ by perturbing one concentration $c_i$.

    \item Growth Adaptation Coefficients $A$: change in \emph{optimal} $\mu^*$ by changing parameters.
\end{itemize}
\begin{columns}
    \begin{column}{0.5\textwidth}  
    \centering
    \small E.g.: changing in the density $\rho$
    \begin{equation*}
    A_\rho = \frac{\rho}{\mu^*}\frac{{\rm d} \mu^*}{{\rm d} \rho} = \frac{\rho}{c_{\rm p}}\left( 1 -  \mu \sum_j \tau_j W^j_{\rm p} \right)
    \end{equation*}
    Comparing to \textit{E. coli} data$^7$ $\rightarrow$
    \end{column}
    \begin{column}{0.5\textwidth}  
        \begin{figure}
            \includegraphics[width=0.55\linewidth]{density.png}
        \end{figure}
    \end{column}
    
\end{columns}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The general GBA problem: formulation on $\mathbf{f}$}

\textbf{For some given model $(\mathbf{M}, \bm{\tau}, \rho)$ and environment $\mathbf{x}$:}
\begin{align*}
    & \underset{\mathbf{v} \, \in \, \mathbb{R}^{\rm r},\mathbf{c} \, \in \, \mathbb{R}^{\rm p}}{\text{maximize}} && \mu ~~~ &&\text{(Maximize growth rate)}\\
    & \text{subject to:} \\
    &&& \mathbf{M} \, \mathbf{v} = \mu \, \mathbf{c}  ~~~ &&\text{(Flux balance)} \\
    &&& c_{\rm p} = \mathbf{v}^\top \bm{\tau}(\mathbf{c},\mathbf{x})    ~~~ &&\text{(Reaction kinetics and protein sum)} \\
    &&& \rho = \sum \mathbf{c} ~~~ &&\text{(Constant cell density)}\\
    &&& \mathbf{v} \odot \bm{\tau}(\mathbf{c},\mathbf{x}) \geq \mathbf{0} ~~~ && \text{(non-negative protein concentrations)} \\
    &&& \text{\sout{$\mathbf{c} \geq \mathbf{0}$}} ~~~ &&\text{\st{(non-negative reactant concentrations)}}
\end{align*}

\bigskip

\textbf{Main trick for analytical ``solution'':} let's define the ``flux fractions''
\begin{equation*}
     \mathbf{f} := ´\frac{\mathbf{v}}{\mu \, \rho} ~~~ \left(=\frac{\mathbf{v}}{v_{\rm uptake}}, \text{kind of ``mass yield'' w.r.t. net mass uptake, adimensional}\right)
\end{equation*}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The general GBA problem: formulation on $\mathbf{f}$}

\begin{block}{General formulation on $\mathbf{f}$ in few steps}
	
\textbf{Substituting}  $\mathbf{v} = \mu \, \rho \, \mathbf{f}$ ~ into ~ $\mathbf{M} \, \mathbf{v} = \mu \, \mathbf{c}$
\begin{equation*} 
   \rho \, \mathbf{M} \, \mathbf{f} = \mathbf{c} ~~~ \text{(independent of $\mu$)}.
\end{equation*}

\textbf{Substituting}  $\mathbf{c} = \rho \, \mathbf{M} \, \mathbf{f}$ ~ into ~ $c_{\rm p} = \mathbf{v}^\top \bm{\tau}(\mathbf{c},\mathbf{x})$
\begin{equation*} \label{eq:cpc}
    M^{\rm p}_{\rm r} f_{\rm r} = \mu \, \mathbf{f}^\top \bm{\tau}(\rho \, \mathbf{M} \, \mathbf{f},\mathbf{x})
\end{equation*}
\textbf{Solving for $\mu$}: 
\begin{equation*}
    \boxed{\mu(\mathbf{f},\mathbf{x}) = \frac{M^{\rm p}_{\rm r} f_{\rm r} }{\displaystyle \mathbf{f}^\top \bm{\tau}(\rho \, \mathbf{M} \, \mathbf{f},\mathbf{x})} }
\end{equation*}
\textbf{The density constraint:}
\begin{equation*}
    \rho = \sum \mathbf{c} ~~ \Leftrightarrow ~~  \boxed{\mathbf{a}^\top \mathbf{f} = 1 } 
\end{equation*}

\end{block}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The general GBA problem: analytical ``solution''}

\textbf{Reformulated problem:} for some given model $(\mathbf{M}, \bm{\tau}, \rho)$ and environment $\mathbf{x}$
\begin{equation*} \label{eq:optimization_f}
    \begin{aligned}
    & \underset{\mathbf{f} \, \in \, \mathbb{R}^{\rm r}}{\text{maximize}} && \mu(\mathbf{f},\mathbf{x}) = \frac{M^{\rm p}_{\rm r} \,f^{\rm r}}{\mathbf{f}^\top \bm{\tau}(\rho \, \mathbf{M} \, \mathbf{f},\mathbf{x}) } \\
    & \text{subject to:} \\
    &&& \mathbf{a}^\top \mathbf{f} = 1 \\
    &&&  \mathbf{f} \odot \bm{\tau}(\rho \, \mathbf{M} \,\mathbf{f},\mathbf{x})  \geq \mathbf{0} ~~~.
    \end{aligned}
\end{equation*} 

\textbf{Analytical conditions for optimal states:} using KKT conditions, we find 
\begin{equation} \label{eq:balance_f}
    \boxed{\left( M^{\rm p}_j - \mu \, \tau_j - \mu \, \mathbf{f}^\top \frac{\partial \bm{\tau}}{\partial f^j} + \mu \, \mathbf{f}^\top \, \frac{\partial \bm{\tau}}{\partial \mathbf{f}} \, \mathbf{f} \, a_j \right) f_j = 0 ~~~ \forall ~ j}
\end{equation}

\textbf{Using $\mathbf{a}^\top \mathbf{f} = 1$:} we have $\rm r - 1$ algebraic equations on $\rm r - 1$ variables (solvable).

    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The general GBA problem: economics}

\textbf{Substituting $\mathbf{v} = \mu \, \rho \, \mathbf{f}$ and $\mathbf{c} = \rho \, \mathbf{M} \, \mathbf{f}$ into the solution (\ref{eq:balance_f}), we find\footnote{\tiny Dourado et al. Mathematical properties of optimal fluxes in cellular reaction networks at balanced growth, \textit{PLOS Comp Biol} 2023.}}
\begin{equation*} 
     {\color{EPCPpurple} M^{\rm p}_j } {\color{red} \, - \,  \mu \, \tau_j} {\color{blue} \, - \, \mathbf{v}^\top  \frac{\partial \bm{\tau}}{\partial \mathbf{c}} \mathbf{M}_j} {\, \color{INRIAgreen} + \,  a_j \, \mathbf{v}^\top \frac{\partial \bm{\tau}}{\partial \mathbf{c}} \, \mathbf{c} /\rho} = 0 ~~~ \forall ~ j
\end{equation*}
 

\bigskip
 
\begin{block}{Economics analogy: the marginal value of each flux fraction $f_j$}
\footnotesize
	{\color{EPCPpurple} $\underbrace{\text{protein production benefit}}_\text{(increased protein production)}$} + {\color{red} $\underbrace{\text{protein cost}}_\text{(protein in $j$)}$} + {\color{blue} $\underbrace{\text{kinetic benefit}}_\text{(protein saved)}$} +  {\color{INRIAgreen} $\underbrace{\text{biomass production benefit}}_\text{(increased biomass production)} $} (= 0 if opt.)
\end{block}

    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Grow Control Analysis: Grow Adaptation Coefficient for $k_{\rm cat}$}

\textbf{We can show from first principles (using the Envelope Theorem)\footnote{\tiny Dourado et al. Mathematical properties of optimal fluxes in cellular reaction networks at balanced growth, \textit{PLOS Comp Biol} 2023.} that:}

\bigskip

\begin{equation*}
    A_{k^j_{\rm cat}} = \frac{{k^j_{\rm cat}} }{\mu^*}\frac{{\rm d} \mu^*}{{\rm d} {k^j_{\rm cat}} } = \phi_j
\end{equation*}

\bigskip

\bigskip

Proportional change in $\mu^*$ is exactly the same as proportion of protein allocated to $j$.
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Summary}

\begin{itemize}\setlength\itemsep{1.5em}
    \item \textbf{GBA: self-replicating models on independent variables, easier to study.}  

    \item \textbf{Analytical conditions for optimal balanced growth (fundamental principles).} 

    \item \textbf{Experimental indications that cells do implement near optimal strategies.} 

    \item \textbf{Proteins emerge as the ``currency'' in cell economics from first principles.}
    
\end{itemize}

\bigskip

\bigskip

\centering
\textbf{(soon chapter in the EPCB book)}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Constraints on GBA}

%\textbf{Numerical solutions for different external concentrations $x$}

\begin{figure}
%\begin{adjustwidth}{-2.5em}{0em}
    \centering
    \includegraphics[width=0.75\linewidth]{constraints.png}
%\end{adjustwidth}
\end{figure}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Michaelis-Menten kinetics with activation}

Based on ``Convenience kinetics''\footnote{\tiny Liebermeister \& Klipp, Bringing metabolic networks to life: convenience rate law and thermodynamic constraints, 2006.}, we define the Michaelis-Menten kinetics with activation, corresponding ``activation constants'' $\mathbf{A}$

\bigskip

\begin{equation*} \label{eq:MMactivation}
    \tau_j= \frac{1}{k^j_{\rm cat}} \prod_m \left(1 + \frac{A^m_j}{c^m} \right) \left(1 + \frac{K^m_j}{c^m} \right) \prod_n \left(1 + \frac{K^n_j}{x^n} \right)
\end{equation*}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Equations for balance growth states: model A}
\scriptsize{
\begin{figure}
\raggedright{\textbf{1) Original problem: Implicit constraints on $\mu$, involving $v_1,v_2,v_3, c_1,c_2,c_3, x_1$ (6 variables, 5 equations)}}
\begin{align*}
    v_1 - v_2 &= \mu \, c_1  \\
    v_2 - v_3 &= \mu \, c_2  &&\textbf{(mass conservation)}  \\
          v_3 &= \mu \, c_3 \\
    \frac{v_1}{6} \left(1 + \frac{1}{x_1} \right) + \frac{v_2}{6} \left(1 + \frac{22}{c_1} \right) + \frac{v_3}{5} \left(1 + \frac{40}{c_2} \right) &= c_3  &&\textbf{(kinetics and protein sum)}  \\
    c_1 + c_2 + c_3 &= 340 &&\textbf{(constant cell density)}
\end{align*}
 
\textbf{2) GBA: Explicit constraint on $\mu(f_2,f_3, x_1)$ (from the density constraint $f_1 = 1$)}

\begin{equation*}
    \mu (f_2,f_3, x_1) = \frac{f_3 }{\displaystyle \frac{1}{6} \left(1 + \frac{1}{x_1} \right) + \frac{f_2}{6} \left(1 + \frac{22}{340(1 - f_2)} \right)  + \displaystyle \frac{f_3}{5} \left(1 + \frac{40}{340 (f_2 - f_3)} \right) } ~~~ \textbf{(constrained growth rate)}
\end{equation*} % f_1 = \frac{1 - u_h \, q^h }{a_1} = 1 , qdot_1 = 0

\smallskip

\textbf{3) Analytical conditions for optimal balanced growth state (system of algebraic equations)}
\begin{align*}
    {\color{red}\frac{1}{6} \left(1 + \frac{22}{340 (1 - f_2)} \right)} {\color{blue} \, + \,  \frac{22 f_2}{6\left[340 (1 - f_2)\right]^2} \,  - \,  \frac{ 40 f_3}{5\left[340 (f_2 - f_3)\right]^2}} &= 0 ~~~~ (j = 2)\\
    {\color{EPCPpurple}1} {\color{red}\, - \,  \mu \frac{1}{5} \left(1 + \frac{40}{340 (f_2 - f_3)} \right)} {\color{blue} \, - \mu \,  
  \frac{40 f_3}{5\left[ 340 (f_2 - f_3)\right]^2}}  &= 0 ~~~~ (j = 3)\\
\end{align*}
\end{figure}
}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Optimal substrate mass concentration = free enzyme mass concentration }

The optimal mass concentration balance for minimal $\rho:$
\begin{equation*} 
    c_m = \frac{p^j \, K^j_m}{\displaystyle K^j_m + c_m} ~~~.
\end{equation*}

But this corresponds exactly to the free enzyme mass concentration
\begin{equation*} 
    p^j_{\rm free} \coloneqq p^j - p^j \left(\frac{c_m}{c_m + K^j_m} \right) = \frac{p^j \, K^j_m}{\displaystyle K^j_m + c_m} ~~~.
\end{equation*}

Thus\footnote{\tiny Dourado et al. On the optimality of the enzyme–substrate relationship in bacteria, \textit{PLOS Biology} 2021}, 
\begin{equation*} 
    c_m = p^j_{\rm free} ~~~.
\end{equation*}


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Numerical solutions for different external concentrations $x$: model B}

\begin{figure}
\begin{adjustwidth}{-2.5em}{0em}
    \centering
    \includegraphics[width=1.05\linewidth]{simulationB.png}
\end{adjustwidth}
\end{figure}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The dynamic generalization: fitness optimization}

\textbf{For some given model $(\mathbf{M}, \bm{\tau}, \rho)$ and {\color{red}dynamic environment $\mathbf{x}(t)$}:}
\begin{align*}
    & \underset{\displaystyle \mathbf{v}(t),\mathbf{c}(t)}{\text{maximize}} && \color{red} \int^T_0 \mu \,{\rm d} t~~~ &&\text{\color{red}(Maximize fitness)}\\
    & \text{subject to:} \\
    &&& \mathbf{M} \, \mathbf{v} = \mu \, \mathbf{c} {\color{red} \, + \, \Dot{\mathbf{c}}}  ~~~ &&\text{(Mass conservation)} \\
    &&& c_{\rm p} = \mathbf{v}^\top \bm{\tau}(\mathbf{c},\mathbf{x})    ~~~ &&\text{(Reaction kinetics and protein sum)} \\
    &&& \rho = \sum \mathbf{c} ~~~ &&\text{(Constant cell density)}
\end{align*}

\bigskip

\textbf{Main trick for analytical ``solution'':} define the ``generalized fluxes'' $\mathbf{q}$ such that
\begin{equation*}
     \rho \, \mathbf{M} \, \mathbf{q} = ´\mathbf{c} ~~~,
\end{equation*}

then reformulate the problem on $\Dot{\mathbf{q}},\mathbf{q},\mathbf{x}$, and solve Euler-Lagrange equations.

\end{frame}

\end{document}
\documentclass[aspectratio=169]{beamer}

\usepackage{ifluatex}
%
\title{Diversity of metabolic flux distributions}
\author{Roberto Mulet}
\date{March 1, 2024}

%
\definecolor{piggyred}{RGB}{255,212,213}
\definecolor{EPCPpurple}{RGB}{105,88,153}
\definecolor{EPCPpurpleMedium}{RGB}{159,139,213}
\definecolor{EPCPpurpleDark}{RGB}{31,0,81}
\definecolor{EPCPocher}{RGB}{254,244,254}
\definecolor{INRIAgreen}{RGB}{0,163,166}

\setbeamercolor{EPCP}{fg=white,bg=EPCPpurpleMedium}
\usecolortheme[named=EPCPpurple]{structure}
%
\setbeamerfont{title}{size=\huge}%
\setbeamercolor{title}{fg=EPCPpurpleDark}
%
\setbeamerfont{author}{size=\large}%
\setbeamercolor{author}{fg=EPCPpurpleDark}
%
\setbeamercolor{frametitle}{fg=black}%
\setbeamercolor{footline}{fg=white,bg=EPCPpurpleMedium}
\setbeamercolor{itemize item}{fg=EPCPpurple}
\setbeamercolor{enumerate item}{fg=EPCPpurpleDark}
%
\setbeamercolor{block title}{fg=white,bg=EPCPpurpleDark}%
\setbeamercolor{block body}{fg=black,bg=EPCPocher!5}%
\setbeamercolor{block title example}{fg=EPCPocher!5,bg=INRIAgreen}%
\setbeamercolor{block body example}{fg=EPCPpurple,bg=INRIAgreen!5}%
\setbeamercolor{block title alerted}{fg=red!50!black,bg=piggyred}%
\setbeamercolor{block body alerted}{fg=black,bg=white}%
%
\setbeamercolor{frametitle}{fg=black}%
%
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamertemplate{navigation symbols}{}

%
\usefonttheme{professionalfonts}
\ifluatex
\usepackage{fontspec}
%\setmainfont{garamond}
\usepackage{unicode-math}
%\setmathfont{garamond}
\else
\usepackage[utf8]{inputenc}
\fi
%


\usepackage[version=4]{mhchem}
\usepackage[square,numbers]{natbib}
%\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{subcaption}
\usepackage{graphicx}

%\makeatletter
\mode<all>{
\ifpdf
\pgfdeclareimage[height=0.2\paperheight]{piggyBac}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=2.3ex]{piggyBacSmall}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=0.05\paperheight]{INRIA}{images/inria-logo.jpg}%
\pgfdeclareimage[height=0.05\paperheight]{MathNum}{images/mathnum-purple.jpg}%
\pgfdeclareimage[height=0.08\paperheight]{Eurip}{images/eurip-purple.jpg}%
\pgfdeclareimage[height=0.11\paperheight]{LPI}{images/LPI-purple.png}%
\pgfdeclareimage[height=2.3ex]{LPISmall}{images/Learning_Planet_Institute_LPI-sm.png}%
%
\else
% TODO? eps version
\fi

\setbeamertemplate{title page}{
\begin{beamercolorbox}[wd=\paperwidth]{EPCP}
\parbox[b]{.12\paperwidth}{\hspace*{.5em}\\[-1ex]\mbox{}}
\hfill
\begin{minipage}[b][0.2\paperheight]{.6\paperwidth}{%
		\begin{center}\LARGE
			Economic Principles in Cell Physiology\\[3mm]
			{\footnotesize \color{EPCPpurpleDark}Paris, July 8--11, 2024\\[-2ex]\mbox{}}
		\end{center}
	}
\end{minipage}
\hfill
%\pgfuseimage{piggyBac}
\parbox[b]{.04\paperwidth}{\hspace*{.5em}\\[-1ex]\mbox{}}
\end{beamercolorbox}

\vskip0pt plus 1filll
\begin{center}
{\usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle}\\
\vspace{1cm}
{\usebeamerfont{author}\usebeamercolor[fg]{author}\insertauthor}
\end{center}

\mbox{}
\vskip0pt plus 1filll
\begin{beamercolorbox}[wd=\paperwidth]{title}
  \hspace{1cm}\pgfuseimage{INRIA}\hspace{.5cm}\pgfuseimage{MathNum}
  \mbox{}\hfill
  \pgfuseimage{Eurip}\hspace{.5cm}\pgfuseimage{LPI}\hspace{1cm}\mbox{}\hfill\\[4mm]
\end{beamercolorbox}
}% end title page
%
\setbeamertemplate{footline}{
	\begin{beamercolorbox}[wd=\paperwidth]{footline}
		\hfill
		{\mbox{}\raise1.3ex\hbox{\insertshorttitle\hspace*{1em}\insertframenumber\slash\inserttotalframenumber\hspace*{.2em}}}
		%\pgfuseimage{piggyBacSmall}
	\end{beamercolorbox}
}% end footline
}% end mode<all>

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START PRESENTATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
	
{\setbeamercolor{background canvas}{bg=EPCPocher}
\begin{frame}[plain]
	\titlepage
\end{frame}
%\maketitle
}

\begin{frame}\frametitle{The Problem}
\centering
\includegraphics[width=.50\textwidth,angle=270]{images/Ecoli3.png}\\
\end{frame}

\begin{frame}{The simplest math}
  \only<1>{
    \begin{equation}
      \frac{dX_i}{dt} = \sum_j S_{ij} \nu_j(\vec{X}) \nonumber
    \end{equation}
  \begin{itemize}
    \item $X_i$ concentration of metabolite $i \in [1,..M]$
    \item $\nu_j$ velocity of reaction $j \in [1,..N]$
    \item $S_{ij}$ Stoichiometric Matrix
    \item $N>M$
  \end{itemize}
  }
\end{frame}

\begin{frame}{Stationarity}
  \onslide<1->{
    \begin{equation}
      \frac{dX_i}{dt} = \sum_j S_{ij} \nu_j(\vec{X}) =  0\nonumber
    \end{equation}
    \begin{block}{Constraint modelling}
      \centering {\Large $\mathcal{S}\vec{\nu} = 0 $}
    \end{block}
  }
  \onslide<2>{
    \begin{block}{Constraint modelling}
      \centering {\Large $\mathcal{S}\vec{\nu} = \vec{b} $}
    \end{block}
  } 
\end{frame}

\begin{frame}{Graphical Representation}
\centering
\includegraphics[width=.50\textwidth]{images/convex-01-cropped.jpg}\\
\end{frame}



\begin{frame}{Additional Assumption}
  \onslide<1->{
    \begin{itemize}
      \item Maximize: $E = \sum_j h_j \nu_j \nonumber$
    \end{itemize}
  }
  \onslide<2>{
    \begin{block}{Flux Balance Analysis = Linear Programming}
      \centering
          {\Large $\mathcal{S}\vec{\nu} = \vec{b} $

            $\max_{\vec{\nu}} E$          }
    \end{block}
  } 
\end{frame}

\begin{frame}{Graphical Representation}
\centering
\includegraphics[width=.50\textwidth]{images/convex-02-cropped.jpg}\\
\end{frame}

\begin{frame}\frametitle{Experimental Support}
  
\centering
\includegraphics[width=.50\textwidth]{images/Palsson.png}\\
\end{frame}


\frame{
  \frametitle{But Life is more complex than that}
  \only<1>{
    \centering
    \includegraphics[width=.60\textwidth]{images/Daniele.png}

    {\tiny V. Onesto el al, Probing Single-Cell Fermentation Fluxes and Exchange Networks via pH-Sensing Hybrid Nanofibers, ACS Nano 2023, 17, 4, 3313–3323}
  }
  \only<2>{
    \centering
    \includegraphics[width=.60\textwidth]{images/Yeast.png}

    {\tiny A. Traven et al, Transcriptional profiling of a yeast colony provides new insight into the heterogeneity of multicellular fungal communities. PLoS One. 2012;7(9):e46243.}
  }
  \only<3>{
    \centering
    \includegraphics[width=.50\textwidth]{images/Review2Hetero.png}\\
    \includegraphics[width=.50\textwidth]{images/Review1Hetero.png}\\
    \includegraphics[width=.50\textwidth]{images/Review3Hetero.png}
  }
}



\begin{frame}{Building a Probability Measure}
  \only<1>{
    \includegraphics[width=.50\textwidth,angle=90]{images/equation1-cropped.jpg}
  }
  \only<2>{
    \includegraphics[width=.50\textwidth,angle=90]{images/equation2-cropped.jpg}
  }
  \only<3>{
    \includegraphics[width=.70\textwidth]{images/equation3-cropped.jpg}
  } 
\end{frame}


\begin{frame}{Maximum Entropy Principle}
  \only<1>{
    \begin{block}{ $S = - \int_\mathcal{P}  P(\nu) \log P(\nu)$}

        Among all the probability densities compatible with the data (or knowledge), the one having the largest value of $S$ is the one that best represents our knowledge of the system
      
    \end{block}
  }
  
\end{frame}

\begin{frame}{Derivation}
  \onslide<1->{
    $max_{P(\nu)} - \large[ \int_\mathcal{P}  P(\nu) \log P(\nu) \large]$
  }
  \onslide<2>{ 
    \vspace{1cm}

    subject to:
    \vspace{1cm}
     $ \int_\mathcal{P}  P(\nu) = 1$    \hspace{1cm} and \hspace{1cm}    $ \int_\mathcal{P}  f(\nu) P(\nu) = < f >$
     
   }
\end{frame}


\begin{frame}{Derivation}
  \onslide<1->{
    \begin{equation}
      \mathcal{L} = - \int_\mathcal{P}  P(\nu) \log P(\nu)
    -\alpha ( \int_\mathcal{P}  P(\nu) - 1 )
    -\beta ( \int_\mathcal{P}  f(\nu) P(\nu) - < f > ) \nonumber
    \end{equation}
  }
  \onslide<2->{
    \begin{equation}
      P(\nu) \sim e^{\beta f(\nu) } \nonumber
      \end{equation}
  }
\end{frame}


\begin{frame}{Summarizing}
\centering
\includegraphics[width=.85\textwidth]{images/ExponentialSpace.png}\\
\end{frame}


\frame
{
  \frametitle{Problem One}
\begin{figure}
\centering
\includegraphics[scale=0.6]{images/tank.pdf}
\end{figure}
  
}

\frame
{
\frametitle{Mathematical framework}
\begin{minipage}[b]{2.0in}
  \begin{center}
 %     \textcolor{red}{
        \begin{equation}
          \frac{d X}{dt} = (\textcolor{red}{\mu} - \sigma - D) X \nonumber
        \end{equation}\\
        \begin{equation}
          \textcolor{red}{\mu} = \mu(\textcolor{red}{u},r) \hspace{1cm} \sigma = \sigma(s) \nonumber
        \end{equation}\\
        \begin{equation}
          \frac{d s_i}{dt} = - \textcolor{red}{u_i} X - (s_i - c_i) D \nonumber
        \end{equation}
 %     }
  \end{center}
\end{minipage}
\begin{minipage}[b]{2.0in}
  \begin{center}
    \begin{equation}
      lb_k \leq r_q \leq ub_{k}  \nonumber
    \end{equation}
    \begin{equation}
      -L_i \leq u_i \leq min\{V_i,c_i \frac{D}{\textcolor{red}{X}}\} \nonumber
    \end{equation}
    \begin{equation}
      \sum_k r_k < K \nonumber 
    \end{equation}
    \begin{equation}
      \sum_k S_{ik} r_k -e_i - y_i \textcolor{red}{\mu}  + \textcolor{red}{u_i} = 0  \nonumber
    \end{equation}
    \begin{center}\textcolor{red}{The cell maximizes biomass production $\mu$} \end{center}
  \end{center}
  \end{minipage}
}

\frame
{
 \frametitle{Small Network}

\centering
\includegraphics[width=.5\textwidth]{images/vazquez-net.pdf}

{\tiny {\it Vazquez et al.. Macromolecular crowding explains overflow metabolism in cells.  Scientific Reports 6, 31007 (2016)}}
}


\frame
{
 \frametitle{Toxicity is the key point}

\centering
\includegraphics[width=1.00\textwidth]{images/vazquez-bif.pdf}
 
}

\frame{
 \frametitle{General Picture}

\centering
\includegraphics[width=.80\textwidth]{images/modes.pdf}

{\tiny
\textbf{(a)} Overflow. At high enough nutrient uptake the respiratory flux hit
s the upper bound $r_\mathrm{max}$ and the remaining nutrients are exported as  $W$. \textbf{(b)} Respiration. The nutrient is completely oxidized with a
large energy yield. \textbf{(c)} Threshold values of $\xi$. $\xi_0$ delimits the nutrient excess regime ($\xi < \xi_0$) from the competition regime ($\xi > \xi_0$). $\xi_\mathrm{sec}$ delimits the transition between overflow metabolism ($\xi < \xi_\mathrm{sec}$ and respiration ($\xi > \xi_\mathrm{sec}$). Finally, maintenance demand cannot be met beyond $\xi > \xi_\mathrm{m}$.
}
}


\frame
{
  \frametitle{Homogeneous Chemostat}
  \onslide<1->{
  \begin{equation}
          \frac{d X}{dt} = (\mu - \sigma - D) X   =  0 \nonumber
  \end{equation}\\
  }
  \onslide<2>{
    \begin{center}
      \begin{equation}
        \textit{If stationary }
        \mu(u,r) - \sigma(s^*) = D \nonumber
      \end{equation}
    \end{center}
  }
}

\frame{
\frametitle{Hetergeneous Chemostat}
  \begin{center}
    \begin{equation}
      D = \frac{X}{\xi} = \int_{\textcolor{red}{\Pi}} ( \mu(\nu)-\sigma(s^*) )P_{s^*} (u,r) d(u,r) \nonumber
    \end{equation}
    \begin{equation}
      s_i^{*} = c_i - \xi \int_{\textcolor{red}{\Pi}} u_i(\nu) P_{s^*} d(u,r)  \nonumber
    \end{equation}
  \end{center}
}


\subsection{Results}
\frame
{
 \frametitle{Small Network again}

\centering
\includegraphics[width=.5\textwidth]{images/vazquez-net.pdf}
 
}

\frame
{
  \frametitle{Effect of the heterogeneity}
  \onslide<1>{
    \centering
    \includegraphics[width=.5\textwidth]{images/fig3.pdf}
  }
}


\frame
{
  \frametitle{Genome Scale Metabolic Network}
   \onslide<1>{
     \centering
     \includegraphics[width=.8\textwidth]{images/XvsD-CHO.png}
     \vspace{2cm}
            {\tiny {\it J. Fernandez-de-Cossio Diaz, K. Le\'on and {\bf R. M.}, Characterizing stationary states of genome scale metabolic networks in continuous culture, PLOS Computational Biology. {\bf 13} (11): e1005835 (2017)}}
   }
}

\frame{
  \frametitle{Experimental results}

  \only<1>{
  \centering
  \includegraphics[width=1.00\textwidth]{images/lisandra.png}
  }
  \onslide<2>{
  \centering
  \includegraphics[width=.70\textwidth]{images/lisandra2.png}
  }

{\tiny   {\it L. Calzadilla-Rosado, E. Hern\'andez, J. Dustet, J. Fern\'andez-de-Cossio-D\'{\i}az, M. Pietzke, A. Vazquez, K. Le\'on, {\bf R. M.} and T. Boggiano, Multiple steady states and metabolic switches in continuous cultures of HEK293: Experimental evidences and metabolomics analysis, Biochemical Engineering Journal {\bf 198}, 109010 (2023)}}
}




\frame
{\frametitle{Problem Two}
  \textcolor{red}{Can we estimate the fluxes of the cultures in a chemostat?}
}

\frame{
\frametitle{Standard approach}
\only<1>{
  \centering
  Given some constraints:  $\mathcal{S}\vec{\nu} = \vec{b}$

  \begin{equation}
     <v_i>_{exp} \in \textrm{polytope} \nonumber
  \end{equation}
  Find $\underline{v}$ such that some function $f(\underline{v})$ is maximum
  }
}

\frame{
\frametitle{Alternatively: Maximum Entropy}
\only<1>{
  \centering
  Given some constraints: $\mathcal{S}\vec{\nu} = \vec{b}$ and:
  
  \begin{equation}
     <v_i>_{exp} = \int \underline{v}P(\underline{v}) dv \in \textrm{polytope} \nonumber
  \end{equation}
  %$\mathop{\textrm{argMaximize}}_{P(\underline{v})} S = - \int P(\underline{v}) \log P(\underline{v})dv$
  $\underset{P(\underline{v})}{\mathrm{argMaximize}} S =  - \int P(\underline{v}) \log P(\underline{v})dv$
  }
}

\frame{
  \frametitle{Toy model}
  \only<1>{
      \begin{center}
        \begin{equation}
          \frac{d X}{dt} = (\textcolor{red}{\mu}  - D) X \nonumber
        \end{equation}\\
        \begin{equation}
          \textcolor{red}{\mu} = \mu(\textcolor{red}{u},r) \nonumber
        \end{equation}\\
        \begin{equation}
          \frac{d s}{dt} = - \textcolor{red}{u} X + (c_R - s) \nonumber
        \end{equation}
  \end{center}
  }
}


\frame{
  \frametitle{Toy model}
  \only<1>{
    \begin{eqnarray}
      \frac{dX(\mu, u_g, u_o)}{dt} &=&  \mu \, X(\mu, u_g,u_o)- D \, X(\mu, u_g, u_o) \nonumber \\
\frac{ds_g}{dt} &=& - \int\limits_{V} u_g, X(\mu,u_g,u_o) \, d\mu \, du_g \, du_o + (c_g - s_g) \, D \nonumber 
\end{eqnarray}
  }
}




\frame{
  \frametitle{Toy model}
  \only<1>{
    \begin{eqnarray}
      \frac{dX(\mu, u_g, u_o)}{dt} &=& (1 - \epsilon) \, \mu \, X(\mu, u_g,u_o)- D \, X(\mu, u_g, u_o) +\nonumber \\
      &+&
      \frac{\epsilon}{V} \int\limits_{V} \mu'  \, X(\mu',u_g', u_o') \, d\mu',du_g'du_o' \nonumber \\
\frac{ds_g}{dt} &=& - \int\limits_{V} u_g, X(\mu,u_g,u_o) \, d\mu \, du_g \, du_o + (c_g - s_g) \, D \nonumber 
\end{eqnarray}
  }
  \onslide<2->{
    \centering
    \includegraphics[width=0.75\textwidth]{images/dyn_Jose2.png}
  }
}


\frame{
  \frametitle{Inference in heterogeneous metabolism}
  %\only<1>{
   % Constraints:
    
    %\begin{equation}
     % D = \bar{\mu} = \int_V d u_g d u_o d \mu \mu X(\mu, u_g, u_o) \nonumber
    %\end{equation}
    
    %\begin{equation}
     % \bar{u_g} \leq c_g \frac{D}{X} \nonumber
    %\end{equation}

    %\begin{equation}
     % P(z,u_g,u_o|\beta_\mu,\beta_g) \sim e^{\beta_\mu \mu + \beta_g u_g}\delta(\dots) \nonumber
    %\end{equation}
  %}
  \onslide<1->{
    \centering
    \includegraphics[width=.35\textwidth]{images/Fluxes_infered.png}
  }
}


\frame
{
 \frametitle{Results for the Genome Scale Ecoli model}
 \only<1>{
   \centering
   \includegraphics[width=.5\textwidth]{images/Fluxes_Ecoli.png}
 }
 \onslide<2>{
   \centering
   \includegraphics[width=.6\textwidth]{images/All_infer.png}
 }
 
{\tiny {\it J.A. Pereiro-Morej\'on, J. Fern\'andez-de-Cossio D\'{\i}az and {\bf R.M}, Inferring metabolic fluxes in nutrient-limited continuous cultures: A 
Maximum Entropy Approach with minimum information,  iScience {\bf 25}, 105450 (2022)}} 
}


\frame
{
 \frametitle{Results for the Genome Scale Ecoli model}
 \only<1>{
   \centering
   \includegraphics[width=.7\textwidth]{images/MEvsFBA.png}
 }
  
 {\tiny {\it D. De Martino et al}, Statistical mechanics for metabolic networks during steady state growth, Nat. Comm. {\bf 9}, 2988 (2018)}
 }


\frame{
  \frametitle{Conclusions}
  \only<1>{
    \begin{itemize}
    \item Every {\em direct} problem can be studied in a probabilistic setting
    \item This probabilistic setting can be used to solve every {\em inverse} problem
    \item There is a lack of experimental results
    \end{itemize}
  }
}





\end{document}

\documentclass[aspectratio=169]{beamer}

\usepackage{ifluatex}
%
\title{Cost of metabolic pathways}
\author{Elad Noor \&{} Wolfram Liebermeister}
\date{July 5, 2022}



%
\definecolor{piggyred}{RGB}{255,212,213}
\definecolor{EPCPpurple}{RGB}{105,88,153}
\definecolor{EPCPpurpleDark}{RGB}{31,0,81}
\definecolor{EPCPocher}{RGB}{254,244,254}
\definecolor{INRAEgreen}{RGB}{0,163,166}
\setbeamercolor{EPCP}{fg=white,bg=EPCPpurple}
\usecolortheme[named=EPCPpurple]{structure}
%
\setbeamerfont{title}{size=\huge}%
\setbeamercolor{title}{fg=EPCPpurpleDark}
%
\setbeamerfont{author}{size=\large}%
\setbeamercolor{author}{fg=EPCPpurpleDark}
%
\setbeamercolor{frametitle}{fg=black}%
\setbeamercolor{footline}{fg=white,bg=EPCPpurple}
\setbeamercolor{itemize item}{fg=EPCPpurple}
\setbeamercolor{enumerate item}{fg=EPCPpurpleDark}
%
\setbeamercolor{block title}{fg=white,bg=EPCPpurpleDark}%
\setbeamercolor{block body}{fg=black,bg=EPCPocher!5}%
\setbeamercolor{block title example}{fg=EPCPocher!5,bg=INRAEgreen}%
\setbeamercolor{block body example}{fg=EPCPpurple,bg=INRAEgreen!5}%
\setbeamercolor{block title alerted}{fg=red!50!black,bg=piggyred}%
\setbeamercolor{block body alerted}{fg=black,bg=white}%
%
\setbeamercolor{frametitle}{fg=black}%
%
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamertemplate{navigation symbols}{}

%
\usefonttheme{professionalfonts}
\ifluatex
\usepackage{fontspec}
\setmainfont{garamond}
\usepackage{unicode-math}
\setmathfont{garamond}
\else
\usepackage[utf8]{inputenc}
\fi
%

\usepackage[version=4]{mhchem}
\usepackage[square,numbers]{natbib}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{tcolorbox}

%\makeatletter
\mode<all>{
\ifpdf
\pgfdeclareimage[height=0.105\paperheight]{piggyAssemblyLine}{images/piggy_assembly_line.pdf}%
\pgfdeclareimage[height=0.29\paperheight]{piggyBac}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=2.3ex]{piggyBacSmall}{images/piggy_bancteria.pdf}%
\pgfdeclareimage[height=1.05cm]{INRAE}{images/INRAE.png}%
\pgfdeclareimage[height=1.05cm]{LPI}{images/Learning_Planet_Institute_LPI-sm.png}%
\pgfdeclareimage[height=2.3ex]{LPISmall}{images/Learning_Planet_Institute_LPI-sm.png}%
%
\else
% TODO? eps version
\fi

\setbeamertemplate{title page}{
\begin{beamercolorbox}[wd=\paperwidth]{EPCP}
\parbox[b]{.22\paperwidth}{\hspace*{.5em}\\[-1ex]\mathrm{}}
\hfill
\begin{minipage}[b][0.23\paperheight]{.45\paperwidth}{%
		\begin{center}\Large
			\textbf{Economic Principles in Cell Physiology}
			\vfill
			{\footnotesize Paris, July 4--6, 2022\\[-2ex]\mathrm{}}
		\end{center}
	}
\end{minipage}
\hfill
\pgfuseimage{piggyBac}
\end{beamercolorbox}
\vskip0pt plus 1filll
\begin{center}
{\usebeamerfont{title}\usebeamercolor[fg]{title}\inserttitle}\\
\vspace{1cm}
{\usebeamerfont{author}\usebeamercolor[fg]{author}\insertauthor}
\end{center}
\mathrm{}
\vskip0pt plus 1filll
\begin{beamercolorbox}[wd=\paperwidth]{title}
	\pgfuseimage{piggyAssemblyLine}\hfill\mathrm{}\pgfuseimage{LPI}
\end{beamercolorbox}
}% end title page
%
\setbeamertemplate{footline}{
	\begin{beamercolorbox}[wd=\paperwidth]{footline}
		\pgfuseimage{LPISmall}\hfill
		{\mathrm{}\raise1.3ex\hbox{\insertshorttitle\hspace*{1em}\insertframenumber\slash\inserttotalframenumber\hspace*{.2em}}}
		\pgfuseimage{piggyBacSmall}
	\end{beamercolorbox}
}% end footline
}% end mode<all>


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START PRESENTATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

{\setbeamercolor{background canvas}{bg=EPCPocher}
\begin{frame}[plain]
	\titlepage
\end{frame}
%\maketitle
}

\begin{frame}{Outline}
    \tableofcontents[hideallsubsections]
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Costs and Benefits}
\begin{frame}[t]{An economist's perspective}

    \begin{itemize}
        \item<1-> What are the \textbf{costs} associated with keeping an enzyme at a certain level $E$?
        \item<2-> What are the \textbf{benefits} associated with keeping an enzyme at a certain level $E$?
        \item<3-> What are the \textbf{costs} associated with keeping a set of pathway enzymes at a certain level $E_\text{tot}$?
        \item<4-> What are the \textbf{benefits} associated with keeping a set of pathway enzymes at a certain level $E_\text{tot}$?
    \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Resource allocation: ribosomal and metabolic fractions}
\begin{frame}{Cells adapt their macro-composition based on the growth conditions}

    \begin{figure}
        \centering
        \begin{tikzpicture}[thick,scale=1]
            \begin{axis}[
                width=6cm, height=6cm,
                xlabel={Ribosomes (in $\mu$M)},
                ylabel={Protein (in M)},
                ymin=1, ymax=3,
                xmin=20, xmax=70,
                ]
                \addplot [domain=20:70, samples=3, color=blue]{3 - 1.6e-2 * x};
            \end{axis}
        \end{tikzpicture}

        \caption{The concentration of protein ($P$) as a function of the concentration of ribosomes ($R$) in \textit{E. coli}, which follows the line $P = 3.0 \cdot 10^6 - 1.6 \cdot 10^4 R$ \cite{Marr1991}.}
        \label{fig:proteinvsribosomes1}
    \end{figure}

\end{frame}

\begin{frame}{The R and C sections of the proteome}
    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{images/scott_cell_resources}
        \caption{Schematic illustration of the growth model. Figure from \cite{ScottKlumpp2014}.}
        \label{fig:proteinvsribosomes2}
    \end{figure}

\end{frame}

\begin{frame}

    \begin{columns}
    \column{0.5\textwidth}<1->
        \textbf{Resource balance analysis*}
        \begin{small}
        \begin{itemize}
            \item Start with a genome-scale stoichiometric model of a metabolic network
            \item Add reactions that describe tRNA, transcription, translation
            \item Obtain apparent kinetic constants for all reactions (ratio between flux and enzyme cost)
            \item Maximize growth rate (by non-linear optimization)
        \end{itemize}
        \end{small}
        * More on this tomorrow at Anne Goelzer's talk on ``Resource allocation models''.

    \column{0.5\textwidth}<2->
        \textbf{Different approach}
        \begin{small}
        \begin{itemize}
            \item Quantify only the metabolic protein fraction cost
            \item Focus on a single pathway or flux mode (not the entire network)
            \item Try to use all available data about every enzyme
            \item Ask general questions about optimality
        \end{itemize}
        \end{small}
    \end{columns}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Toy examples for flux optimization}

\begin{frame}[t]{Maximizing flux through an unbranched pathway}
    Consider the following linear pathway \cite{Heinrich1991}:
    \[ S_0 \ce{<=>[v_1]} S_1 \ce{<=>[v_2]} \ldots \ce{<=>[v_n]} S_n \]
    where $S_0$ and $S_n$ are the (pre-defined) concentrations of the first substrate and last product, $S_i$ is the (variable) concentrations of intermediate $i$, and $E_i$ is the (variable) concentration of the enzyme catalyzing reaction $i$.

    \only<2->{
         We assume that each reaction follows the rate law $v_i = E_i \cdot (k_i S_{i-1} - k_{-i} S_i)$
        \begin{equation}
            S_{i-1}  \ce{<=>[E_i k_i S_{i-1}][E_i k_{-i} S_i]} S_i
        \end{equation}
    }
    \only<2>{
        \begin{small}
            * This rate law corresponds to unsaturated Michaelis-Menten kinetics, where $k_i = k_\text{cat}/K_M$
        \end{small}
    }
    \only<3->{
        Finally, assume we have a fixed amount of total enzyme concentration ($E_\text{tot}$).
        \begin{eqnarray*}
            \sum_i E_i &\leq& E_\text{tot}
        \end{eqnarray*}
    }
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Constants: $S_0$, $S_n$, $k_i$, $k_{-i}$, $E_\text{tot}$
        \item Variables: $v_i$, $E_i$, $S_i$
        \item Constraints: $v_i = E_i \cdot (k_i S_{i-1} - k_{-i} S_i)$ ~~~~ $\sum_i E_i \leq E_\text{tot}$ ~~~~ $v_i = J$
    \end{itemize}
    \begin{tcolorbox}[colback=green!20!black,colframe=black,title=Blackboard]
        \vspace{4cm}
    \end{tcolorbox}

\end{frame}

\begin{frame}[t]{Maximizing flux through an unbranched pathway}
    At steady-state, all rates must be equal to the pathway flux ($J$):
    \begin{eqnarray*}
        J &=& v_i = E_i \cdot (k_i S_{i-1} - k_{-i} S_i) \\
        E_i &=& \frac{J}{k_i S_{i-1} - k_{-i} S_i}
    \end{eqnarray*}

    \only<2>{\textbf{Conjecture 1:}

        to achieve the highest pathway flux $J$, the optimal enzyme allocation \cite{KacserBurns1981}\cite{Coton2022} is:

        \begin{tcolorbox}[colback=green!5,colframe=green!40!black,title=Optimal allocation]
            \begin{eqnarray*}
                \hat{E_i} &=& E_\text{tot} \cdot \frac{A_i^{-1/2}}{\sum_i A_i^{-1/2}} \\
                A_i &\equiv& \frac{k_1}{k_{-1}} \cdot \frac{k_2}{k_{-2}} \cdots \frac{k_{(i-1)}}{k_{-(i-1)}} \cdot k_i
            \end{eqnarray*}
        \end{tcolorbox}
    }
\end{frame}

\begin{frame}

    \begin{tcolorbox}[colback=green!20!black,colframe=black,title=Blackboard]
        \vspace{6cm}
    \end{tcolorbox}

\end{frame}

\begin{frame}[t]{Maximizing flux through an unbranched pathway}

    First, we solve a simplified version of the conjecture, where all $k_{i} = k_{-i} = 1$.

    \vspace{1cm}

    \textbf{Conjecture 1.1:}
    Given a pathway in steady-state with $n$ reactions described by:
    \begin{eqnarray*}
        J &=& v_i = E_i \cdot (S_{i-1} - S_i) \\
        \sum_i E_i &\leq& E_\text{tot}
    \end{eqnarray*}
    flux is maximized when the enzymes are distributed uniformly along the pathway:
    \begin{eqnarray*}
        \hat{E_i} = E_\text{tot}/n
    \end{eqnarray*}

\end{frame}

\begin{frame}[t]{Maximizing flux through an unbranched pathway}
    \textbf{Proof of Conjecture 1.1:}\\
    First, we note that the constraint on $E_\text{tot}$ must be realized, otherwise we can increase all enzyme amounts proportionally (until reaching the maximum) and thus increase $J$ by the same factor.

    Then, we note that there is a simple relationship between two consecutive metabolites:
    \begin{eqnarray*}
        J &=& E_i \cdot (S_{i-1} - S_i) \\
        S_i &=& S_{i-1} - J/E_i\,.
    \end{eqnarray*}

    Starting with $S_n$ and iteratively substituting $S_i$ using this formula until reaching $S_0$:
    \begin{eqnarray*}
        S_n &=& S_0 - J \sum_i E_i^{-1} \\
        J &=& \frac{S_0 - S_n}{\sum_i E_i^{-1}} \,.
    \end{eqnarray*}
\end{frame}

\begin{frame}[t]{Maximizing flux through an unbranched pathway}
    \textbf{Proof of Conjecture 1.1:}\\
    So, we find that $J$ is proportional to the harmonic mean of $E_i$:
    \begin{equation}
        J = (S_0 - S_n) \cdot \left(\sum_i E_i^{-1}\right)^{-1}
    \end{equation}
    and together with the fact that $\sum_i E_i = E_\text{tot}$, we can see\footnote{Exercise: prove that the harmonic mean is maximized by a uniform distribution.} that the maximum is reached at $E_i = E_\text{tot}/n$.\qed

    \begin{figure}
    \centering
    \begin{tikzpicture}[thick,scale=1]
        \begin{axis}[
            width=6cm,
            height=3cm,
            xlabel={$x$},
            ylabel={$\frac{1}{\frac{1}{x} + \frac{1}{1-x}}$},
            ymin=0, ymax=0.3,
            xmin=0, xmax=1,
            ]
            \addplot [domain=0.001:0.999, samples=50, color=blue]{ 1/(1/x + 1/(1-x)) };
        \end{axis}
    \end{tikzpicture}
    \end{figure}

\end{frame}

\begin{frame}[t]{Maximizing flux through an unbranched pathway}
    Obviously, the assumption that $k_{i} = k_{-i} = 1$ is very oversimplified. Imagine what would happen if:
    \begin{itemize}
        \item<2-> one of the enzymes was much slower (i.e. low values of $k_i$ and $k_{i-1}$)?
        \item<3-> one of the reactions was thermodynamically unfavorable ($k_{-i} \gg k_i$)?
    \end{itemize}
    \only<3>{
    \begin{tcolorbox}[colback=green!20!black,colframe=black,title=Blackboard]
        \vspace{4cm}
    \end{tcolorbox}
    }
\end{frame}

\begin{frame}[t]{Maximizing flux through an unbranched pathway}
    \textbf{Proof of Conjecture 1:}
    We can define the following aggregate kinetic parameters:
    \begin{eqnarray*}
        A_i &\equiv& \frac{k_1}{k_{-1}} \cdot \frac{k_2}{k_{-2}} \cdots \frac{k_{(i-1)}}{k_{-(i-1)}} \cdot k_i
    \end{eqnarray*}
    Then, solving for the flux $J$ given a set of enzyme concentrations $E_i$ \cite{KacserBurns1973}:
    \begin{eqnarray*}
        J &=& \left(S_0 - S_n \prod_{i=1}^n (k_{-i}/k_i)\right)\left(\sum_i (A_i E_i)^{-1}\right)^{-1}
    \end{eqnarray*}

    In other words, $J$ is proportional to a \textit{weighted} harmonic mean of the $E_i$s. This function is maximized (for a given total $\sum_i E_i = \text{const}$) when:
    \begin{eqnarray*}
        E_i \propto A_i^{-1/2}
    \end{eqnarray*}  \qed

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Factorized rate law}
\begin{frame}[t]

    \frametitle{A historical overview of kinetic rate laws}

    \begin{columns}
    \column{0.5\textwidth}
    Consider a reaction $S \ce{<=>} P$ catalyzed by an enzyme $E$.
    \begin{itemize}
    \item<2-> 1913 - Michaelis and Menten \cite{MichaelisMenten1913} were first to suggest a mechanism for Victor Henri's formula \cite{Henri1903} for enyzme inital rate.
    \item<3-> 1930 - Haldane \cite{BriggsHaldane1925,Haldane1930} generalized it to reversible kinetics (and beyond initial rates).
    \item<4-> Based on this derivation, Haldane also found a relationship that always holds between the kinetic parameters of the enzyme and the equilibrium constant of the reaction ($K_\text{eq}$).
    \end{itemize}
    \column{0.5\textwidth}
    \only<2>{
        \begin{tikzpicture}
            \begin{axis}[height=6cm, width=6cm, title={Michaelis-Menten}, grid=major, xlabel={$[S]/K_M$}, ylabel=$v/V_{max}$, xtick={0,1,...,5}, xmin=0, xmax=5, ymin=0, ymax=1, legend entries={$V_\text{max} \frac {[S]}{K_M + [S]}$, $\frac{V_\text{max}}{K_M} [S]$}, legend pos=south east ]
                \addplot [domain=0:5, ] {x/(x+1)};
                \addplot [domain=0:1, color=blue] {x};
            \end{axis}
        \end{tikzpicture}
    }

    \only<3->{

        \begin{eqnarray}
            v = [E_0] \frac {k_\text{cat}^+ [S]/K_S - k_\text{cat}^- [P]/K_P}{1 + [S]/K_S + [P]/K_P}
        \end{eqnarray}
        \begin{tiny}(* for the unimolecular case)\end{tiny}
    }
    \only<4->{
        \begin{eqnarray}
        \frac{k_\text{cat}^+}{k_\text{cat}^-} \cdot \frac{K_P}{K_S} = K_\text{eq}
        \end{eqnarray}
    }
    \end{columns}
\end{frame}



\begin{frame}[t]
    \frametitle{The Factorized Rate Law}

    Using the Haldane relationship, the Haldane rate law can be rewritten\footnote{Exercise: show that equation \ref{eq:factorized} is equivalent to standard Haldane rate law formulation.} in the following form \cite{NoorFlamholz2013}:
    \begin{eqnarray}
        v = [E_0] \underbrace{k_\text{cat}^+ \cdot \left(1 - e^{\Delta_r G'/RT} \right) \cdot \frac{[S]/K_S}{1 + [S]/K_S + [P]/K_P}}_{k_\text{app}} \label{eq:factorized}
    \end{eqnarray}

    where
    \begin{eqnarray*}
        \Delta G_r' &=& \Delta G_r'^\circ + R \cdot T \cdot \ln{\left([P]/[S]\right)} \\
        \Delta G_r'^\circ &=& - R \cdot T \cdot \ln{K_\text{eq}}
    \end{eqnarray*}

    \only<2->{
        Note that $v = [E_0] k_\text{app}$ is the rate law used in Resource Balance Analysis, where $k_\text{app}$ is a constant estimated from empirical data.
    }

\end{frame}

\begin{frame}[t]
    \frametitle{The Factorized Rate Law}
    \begin{eqnarray*}
        v &=& [E_0] k_\text{cat}^+ \cdot \eta^\text{rev} \cdot \eta^\text{sat} \\
        \eta^\text{rev} &\equiv& 1 - e^{\Delta_r G'/RT} \\
        \eta^\text{sat} &\equiv& \frac{[S]/K_S}{1 + [S]/K_S + [P]/K_P}
    \end{eqnarray*}
    * $\eta^\text{rev}$ and $\eta^\text{sat}$ are unitless and range between $(0, 1)$
   % \only<3>{
   %\begin{tiny}
   %$[E_0] k_\text{cat}^+$ = 1 mM/s, $\Delta_r G'^\circ=0$, $K_S = 1$ mM, $K_P = 1$ mM
   %\end{tiny}
   %     \begin{figure}
   %         \centering
   %         \begin{tikzpicture}[thick,scale=1]
   %         \begin{semilogxaxis}[
   %             width=5cm,
   %             height=4cm,
   %             xlabel={$[S]$ (in mM)},
   %             title={$[P] = 100$ mM},
   %             legend entries={$\eta^\text{rev}$,$\eta^\text{sat}$,$v$ (in mM/s)},
   %             ymin=0, ymax=1,
   %             xmin=1, xmax=10000,
   %             legend pos=outer north east,
   %             ]
   %             \addplot [domain=1:10000, samples=50, color=green]{max(0, 1 - 100/x)};
   %             \addplot [domain=1:10000, samples=50, color=blue]{x/(1 + x + 100)};
   %             \addplot [domain=1:10000, samples=50, color=red]{max(0, 1 - 100/x) * x/(1 + x + 100)};
   %         \end{semilogxaxis}
   %         \end{tikzpicture}
   %     \end{figure}
   %
   % }

    \only<2->{
        \begin{columns}
        \column{0.3\textwidth}

            \begin{itemize}
                \item $[E_0] k_\text{cat}^+$ = 1 mM/s
                \item $\Delta_r G'^\circ=0$
                \item $K_S = 1$ mM
                \item $K_P = 1$ mM
            \end{itemize}

        \column{0.7\textwidth}
            \begin{figure}
            \begin{subfigure}{0.4\textwidth}
                \begin{tikzpicture}[thick,scale=0.6]
                    \begin{axis}[
                        width=6cm,
                        height=6cm,
                        xlabel={$\log_{10}([S])$},
                        ylabel={$\log_{10}([P])$},
                        zlabel={$-\log_{10}(\eta^\text{rev})$},
                        %title={$\eta^\text{rev} = 1 - e^{\Delta_r G'/RT}$},
                        view={-35}{45},
                        colorbar,
                        colormap/cool,
                        point meta min = 0,
                        point meta max = 2,
                        colorbar style = {
                            samples = 10,
                            height = 2cm,
                            width = 0.2cm
                        }
                        ]

                        \addplot3 [
                        surf,
                        shader=interp,
                        samples=50,
                        domain=0:2,
                        y domain=0:2,
                        ] {-log10(max(0.01, 1 - 10^(y-x)))};
                    \end{axis}
                \end{tikzpicture}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.4\textwidth}
                \begin{tikzpicture}[thick,scale=0.6]
                    \begin{axis}[
                        width=6cm,
                        height=6cm,
                        xlabel={$\log_{10}([S])$},
                        ylabel={$\log_{10}([P])$},
                        zlabel={$-\log_{10}(\eta^\text{sat})$},
                        %title={$\eta^\text{sat} = \frac{[S]/K_S}{1 + [S]/K_S + [P]/K_P}$},
                        view={-35}{45},
                        colorbar,
                        colormap/cool,
                        point meta min = 0,
                        point meta max = 2,
                        colorbar style = {
                            samples = 10,
                            height = 2cm,
                            width = 0.2cm
                            }
                        ]
                        \addplot3 [
                        surf,
                        shader=interp,
                        samples=50,
                        domain=0:2,
                        y domain=0:2,
                        ] {log10 (1 + 10^x + 10^y) - x)};
                    \end{axis}
                \end{tikzpicture}
            \end{subfigure}
        \end{figure}
    \end{columns}
    }

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Enzyme Cost Minimization (for a single pathway)}

\begin{frame}[t]
    \frametitle{Enzyme Cost Minimization}
    Let's go back to our linear pathway: \[ S_0 \ce{<=>[v_1]} S_1 \ce{<=>[v_2]} \ldots \ce{<=>[v_n]} S_n \]

    \begin{columns}
    \column{0.5\textwidth}<1->
    But now, all enzymes have general kinetics based on the factorized rate law:
    \begin{eqnarray*}
        v_i &=& E_i \cdot k^+_\text{cat, i} \cdot \eta^\text{rev}_i \cdot \eta^\text{sat}_i \\
        \sum_i E_i &\leq& E_\text{tot}
    \end{eqnarray*}
    \column{0.5\textwidth}<2->
    \begin{tcolorbox}[colback=green!5,colframe=green!40!black,title=How to solve]
        In this case, it is not possible to express $J$ as a function of $E_i$. But, we can use the following trick: minimizing $\sum_i E_i$ for a given $J$ is equivalent to maximizing $J$ for a given $\sum_i E_i = E_\text{tot}$. The only free variables will be the metabolite concentrations.
    \end{tcolorbox}
    \end{columns}

\end{frame}

\begin{frame}[t]
    \frametitle{Enzyme Cost Minimization}
    Let's go back to our linear pathway: \[ S_0 \ce{<=>[v_1]} S_1 \ce{<=>[v_2]} \ldots \ce{<=>[v_n]} S_n \]

    \begin{columns}
        \column{0.5\textwidth}<1->
            Given a flux $J$ find the set of metabolite concentrations $\textbf{S}$ that minimizes:
            \begin{eqnarray*}
                \sum_i E_i &=& \sum_i \frac{J}{k^+_\text{cat, i} \cdot \eta^\text{rev}_i \cdot \eta^\text{sat}_i}
            \end{eqnarray*}
            where $\eta^\text{rev}_i$ and $\eta^\text{sat}_i$ are both functions of $\textbf{S}$.
        \column{0.5\textwidth}<2->
        \begin{tcolorbox}[colback=green!5,colframe=green!40!black,title=How to solve]
            Solving this problem analytically is not possible in general, but it can be done numerically using convex optimization \cite{NoorFlamholz2016}.
        \end{tcolorbox}

    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{ECM example for a toy model}

    \begin{figure}
        \centering
        \includegraphics[width=0.8\linewidth]{images/toy\_model\_ecm.pdf}
        \caption{A 3-step toy model showing the enzyme cost as a function of metabolite concentrations.}
        \label{fig:toymodelecm}
    \end{figure}
\end{frame}

\begin{frame}{Examples using Jupyter Notebook}
    \begin{center}
        \href{https://gitlab.com/principlescellphysiology/book-economic-principles-in-cell-biology/-/blob/master/summer-school-2022/lpi-slides/lectures/PAT/exercises/exercise_PAT.ipynb}{\includegraphics[width=4cm]{images/jupyter.pdf} \\ click here}
    \end{center}

\end{frame}


\begin{frame}
    \frametitle{Example of two glycolyses}

    \begin{figure}
        \centering
        \includegraphics[width=0.35\linewidth]{images/ED\_vs\_EMP.pdf}
        \caption{Metabolic network showing both types of glycolysis.}
        \label{fig:ed_vs_emp_network}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A tale of two glycolyses}

    \begin{figure}
        \centering
        \includegraphics[width=1\linewidth]{exercises/emp\_vs\_ed\_demand.pdf}
        \caption{Optimized enzyme concentrations based on ECM results. Pathway flux = $0.01 mM/s$, $k_\text{cat} = 200 s^{-1}$, $K_M = 200 \mu M$, enzyme MW = $40 kDa$}
        \label{fig:ed_vs_emp_enzyme_breakdown}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Running ECM on a model of \textit{E. coli} central metabolism}

    \begin{tiny}
    \begin{table}
    \begin{tabular}{c|c|c|c|c}
        Reaction & flux & $k_\text{cat}^+$ & $K_S$ & $\Delta G'^\circ$ \\
        \hline
        PGI & 0.39 mM/s & 17.8 1/s & 0.15 mM & 2.5 kJ/mol \\
        PFK & 0.44 mM/s & 12.5 1/s & 0.07 mM & -16.1 kJ/mol \\
        FBA & 0.44 mM/s & 19.0 1/s & 0.22 mM & 21.4 kJ/mol \\
        TPI & 0.44 mM/s & 967.7 1/s & 8.43 mM & 5.49 kJ/mol \\
        GAP & 0.92 mM/s & 170.2 1/s & 0.61 mM & 5.23 kJ/mol \\
        &&\vdots&&
    \end{tabular}
    \end{table}
    \end{tiny}

    The data was collected from online databases such as BRENDA and eQuilibrator.
\end{frame}

\begin{frame}
    \frametitle{Optimal enzyme costs can predict actual in vivo concentrations}


    \begin{figure}
    \includegraphics[width=3.5cm]{images/ccm_flux_map.png}
    \includegraphics[width=10cm]{images/ECM_barplot.pdf}
    \caption{A flux map of central metabolism (left) and the optimized enzyme concentrations (right).}
%        \label{fig:ecm_solution}
    \end{figure}
    \begin{tiny}
        * note that enzymes with a small minimial cost (blue bar) tend to have higher thermodynamic (orange) and saturation (brown) costs.
    \end{tiny}
\end{frame}

\begin{frame}
    \frametitle{Optimal enzyme costs can predict actual in vivo concentrations}

    \begin{figure}
        \centering
        \includegraphics[width=0.4\linewidth]{images/ECM\_correlation.pdf}
        \caption{Comparing the optimized enzyme concentrations from ECM to a quantitative proteomics dataset.}
        \label{fig:ecm_validation}
    \end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[t]
    \frametitle{Max-min Driving Force}

    Enzyme Cost Minimization requires full knowledge of all enzyme kinetic parameters, but often our knowledge is limited. One approximation would be to only consider thermodynamic constraints.

    Max-min Driving Force is a method based on the argument avoiding close-to-equilibrium reactions reduces enzyme cost (i.e. $\eta^{rev}$ should be as high as possible).

    \begin{eqnarray*}
        v &=& [E_0] \cdot \underbrace{k_\text{cat}^+}_\text{assume constant} \cdot \underbrace{\left(1 - e^{\Delta_r G'/RT} \right)}_{\eta^{rev}} \cdot \underbrace{\frac{[S]/K_S}{1 + [S]/K_S + [P]/K_P}}_{\text{assume}~~\eta^\text{sat} = 1}
    \end{eqnarray*}
    \only<2>{
    \begin{eqnarray*}
        [E_0] &\propto& (1 - e^{\Delta_r G'/RT})^{-1}
    \end{eqnarray*}
    }
\end{frame}

\begin{frame}[t]
    \frametitle{Max-min Driving Force}

    \begin{columns}
    \column{0.5\textwidth}
        For MDF analysis, we assume that costs are inversly proportional to the thermodynamic term in the factorized rate law.
        \begin{eqnarray*}
            [E_0] &\propto& (1 - e^{\Delta_r G'/RT})^{-1}
        \end{eqnarray*}

        Instead of minimizing the sum of costs (like in Enzyme Cost Minimization), we try to maximize the driving force ($-\Delta_r G'/RT$) of all reactions simultaneously \cite{NoorFlamholz2013}.

    \column{0.5\textwidth}
        \begin{center}
        \begin{tikzpicture}
            \begin{axis}[height=6cm, width=6cm, grid=major, xlabel={$-\Delta_r G'/RT$}, ylabel=$(1 - e^{\Delta_r G'/RT})^{-1}$, xtick={0,2,...,10}, xmin=0, xmax=10, ymin=0, ymax=10]
                \addplot [domain=0.05:10, color=blue] {1/(1-e^(-x))};
            \end{axis}
        \end{tikzpicture}
        \end{center}
    \end{columns}


\end{frame}

\begin{frame}
    \frametitle{Elementary Flux Modes and global optimization of enzyme cost}

    \textbf{Final thoughts}

    In many cases, we don't have only a pair of pathways to choose between. Rather, we have a complex metabolic map with a huge number of possible steady-state flux solutions.

    We will address that scenario in the next talk, given by Meike Wortel.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
    \frametitle{References}

    \begin{tiny}
        \bibliographystyle{unsrtnat}
        \bibliography{bibliography}
    \end{tiny}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Appendix 1: deriving the Haldane rate law}
\begin{frame}[t]
    \frametitle{Quantifying the effects of enzyme and reactant concentration}

    The Haldane derivation is based on this model of catalysis:
    \[ {\rm S} + {\rm E} \ce{<=>[k_{1}][k_{2}]} {\rm ES} \ce{<=>[k_{3}][k_{4}]} {\rm EP} \ce{<=>[k_{5}][k_{6}]} {\rm P} + {\rm E} \]

    \only<2->{
        which translates to the following ODE system:
        \begin{align*}
            \frac{d [ES]}{dt} &= [E]\cdot [S]\cdot k_1 + [EP]\cdot k_4 - [ES]\cdot (k_2 + k_3)
            \\[4pt]
            \frac{d [EP]}{dt} &= [E]\cdot [P]\cdot k_6 + [ES]\cdot k_3 - [EP]\cdot (k_4 + k_5)
            \\[4pt]
            \frac{d [P]}{dt} &= [EP]\cdot k_5 - [E]\cdot[P]\cdot k_6
        \end{align*}
    }
    \only<3->{Haldane assumed the system quickly reaches a quasi-steady-state and therefore all time derivatives are equal to 0. In addition, the total enzyme concentration $[E_0] = [E] + [EP] + [ES]$ does not change over time.}
\end{frame}

\begin{frame}
    \frametitle{Quantifying the effects of enzyme and reactant concentration}

    The easiest way to solve this system of equations is by using a matrix notation:
    \begin{equation}\label{eq:uni_uni_matrix_ode}
        \begin{pmatrix}
            [S] k_1&-(k_2+k_3) & k_4\\
            [P] k_6&k_3 & -(k_4+k_5)\\
            1&1&1
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        \begin{pmatrix}
            0\\
            0\\
            [E_0]
        \end{pmatrix},
    \end{equation}
    where the first two rows of the matrix correspond to $\frac{d [ES]}{dt} = 0$ and $\frac{d [EP]}{dt} = 0$, and the last row represents conservation of total enzyme concentration (note, that the equation $\frac{d [P]}{dt} = 0$ is redundant and therefore not used).

\end{frame}

\begin{frame}
    \frametitle{The reversible Haldane rate law}

    Solving\footnote{Exercise: solve the linear ODE system using Gaussian elimination.} equation \ref{eq:uni_uni_matrix_ode} yields:
    \begin{eqnarray}
        v = [E_0] \frac{k_\text{cat}^+ [S]/K_S - k_\text{cat}^- [P]/K_P}{1 + [S]/K_S + [P]/K_P}
    \end{eqnarray}
    where:
    \begin{eqnarray*}
        K_S &=& \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{k_1(k_3 + k_4 + k_5)} \\
        K_P &=& \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{k_6(k_2 + k_3 + k_4)} \\
        k_\text{cat}^+ &=& \frac{k_3 k_5}{k_3 + k_4 + k_5} \\
        k_\text{cat}^- &=& \frac{k_2 k_4}{k_2 + k_3 + k_4}
    \end{eqnarray*}


\end{frame}

\begin{frame}[t]
    \frametitle{The Haldane relationship}

    In addition, Haldane noticed that there is a dependency between the four kinetic parameters:

    \only<1>{
        \begin{equation}
            \frac{k_\text{cat}^+}{k_\text{cat}^-} \frac{K_P}{K_S} = \frac{k_1 k_3 k_5}{k_2 k_4 k_6}\,.
        \end{equation}
    }

    \only<2->{
        \begin{equation}
            \frac{k_\text{cat}^+}{k_\text{cat}^-} \frac{K_P}{K_S} = \frac{k_1 k_3 k_5}{k_2 k_4 k_6} = K_\text{eq}\,.
        \end{equation}
        According to mass-action kinetics, this fraction is equal to the equilibrium constant of the $S \ce{<=>} P$ reaction.\\}
    \vspace{2cm}
    \only<3->{
        Today, this is commonly known at the \textit{Haldande relationship}. Since $K_\text{eq}$ is a physical constant independent of the enzyme, this means that uni-uni enzyme kinetic parameters have only three degrees of freedom (rather than four).
    }
\end{frame}

\end{document}
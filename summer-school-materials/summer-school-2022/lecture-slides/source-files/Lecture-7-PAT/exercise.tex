\documentclass[12pt,a4paper]{article}
\usepackage[bottom=2cm,top=2cm,left=1cm,right=1cm]{geometry}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[version=4]{mhchem}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{graphicx}

\title{Exercise for Economic Principles in Cell Physiology Summer School 2022}
\author{Elad Noor \&{} Wolfram Liebermeister}
\date{July 5, 2022}

\begin{document}
\maketitle

\section{Questions}
    \subsection{Flux Maximization for a Linear Pathway}

    Prove that the function:
    \begin{equation}
        f(\vec{E}) = \frac{1}{\sum_i (A_i E_i)^{-1}}
    \end{equation}
    for a fixed $\vec{A}$ and under the constraint $\sum_i E_i = E_\text{tot}$, is at its maximum when:
    \begin{eqnarray*}
        E_i = E_\text{tot} \cdot \frac{A_i^{-1/2}}{\sum_i A_i^{-1/2}}
    \end{eqnarray*}

    \subsection{Haldane Kinetic Rate Law}

    Haldane described an enzyme-catalyzed reaction by three steps, each following a mass-action rate law:
    \begin{eqnarray}
        {\rm S} + {\rm E} \ce{<=>[k_{1}][k_{2}]} {\rm ES} \ce{<=>[k_{3}][k_{4}]} {\rm EP} \ce{<=>[k_{5}][k_{6}]} {\rm P} + {\rm E}\,.
    \end{eqnarray}

    The ODE system describing the change in time of each species is:
    \begin{eqnarray}
        \frac{d [ES]}{dt} &=& [E]\cdot [S]\cdot k_1 + [EP]\cdot k_4 - [ES]\cdot (k_2 + k_3) \\
        \frac{d [EP]}{dt} &=& [E]\cdot [P]\cdot k_6 + [ES]\cdot k_3 - [EP]\cdot (k_4 + k_5) \\
        \frac{d [E]}{dt} &=& -[E]\cdot[S]\cdot k_1 + [ES]\cdot k_2 + [EP]\cdot k_5 - [E]\cdot[P]\cdot k_6
    \end{eqnarray}

    Prove that at quasy-steady-state (where the total enzyme concentration is fixed, and the concentration of each species doesn't change over time), the rate in which $[S]$ is converted to $[P]$ is governed by the following rate law:
    \begin{eqnarray}\label{eq:haldane}
        v = [E_0] \frac{k_\text{cat}^+ [S]/K_S - k_\text{cat}^- [P]/K_P}{1 + [S]/K_S + [P]/K_P}
    \end{eqnarray}
    where:
    \begin{eqnarray*}
        K_S &=& \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{k_1(k_3 + k_4 + k_5)} \\
        K_P &=& \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{k_6(k_2 + k_3 + k_4)} \\
        k_\text{cat}^+ &=& \frac{k_3 k_5}{k_3 + k_4 + k_5} \\
        k_\text{cat}^- &=& \frac{k_2 k_4}{k_2 + k_3 + k_4}
    \end{eqnarray*}

    \subsection{The Factorized Rate Law}

    Use the Haldane relationship:
    \begin{eqnarray}
        \frac{k_\text{cat}^+}{k_\text{cat}^-} \frac{K_P}{K_S} &=& \frac{k_1 k_3 k_5}{k_2 k_4 k_6} = K_\text{eq}
    \end{eqnarray}
    and the definition of Gibbs free energy:
    \begin{eqnarray}
        \Delta G_r'^\circ &=& - R \cdot T \cdot \ln{K_\text{eq}}\\
        \Delta G_r' &=& \Delta G_r'^\circ + R \cdot T \cdot \ln{\left([P]/[S]\right)}
    \end{eqnarray}
    to prove that Equation \ref{eq:haldane} is equivalent to the following factorized rate law:
    \begin{equation}
        v = [E_0] k_\text{cat}^+ \cdot \left(1 - e^{\Delta_r G'/RT} \right) \cdot \frac{[S]/K_S}{1 + [S]/K_S + [P]/K_P} \label{eq:factorized}
    \end{equation}

\section{Solutions}

    \subsection{Haldane Kinetic Rate Law}

    First, we add the constraint on the total enzyme concentration ($[E]+[ES]+[EP] = E_\text{tot}$) and rewrite the ODE system in matrix notation:
    \begin{equation}
        \begin{pmatrix}
            1&1&1 \\
            [S] k_1&-(k_2+k_3) & k_4\\
            [P] k_6&k_3 & -(k_4+k_5)\\
            -[S] k_1 - [P] k_6 & k_2 & k_5\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        \begin{pmatrix}
            [E_0]\\
            0\\
            0\\
            0
        \end{pmatrix}.
    \end{equation}
    Note that the last row is linearly dependent on the two previous ones (it is minus their sum). Therefore, we can drop it from the system without loosing information. Then, we will find exlicit expressions for $[E]$, $[ES]$, and $[EP]$ by using Gaussian elimination -- a process of eliminating off-diagonal values in the matrix until we reach the identity matrix, while at the same time applying the same operations to the vector on the right-hand side of the equality.

    Step 1, elimination the off-diagonal elements on the first column (subtracting the first row times $[S] k_1$ from the 2nd row and the first row times $[P] k_6$ from the 3rd row)
    \begin{equation*}
        \begin{pmatrix}
            1 & 1 & 1 \\
            0 & -(k_2+k_3) -[S] k_1 & k_4 -[S] k_1\\
            0 & k_3 -[P] k_6 & -(k_4+k_5) - [P] k_6\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            1\\
            -[S] k_1\\
            - [P] k_6\\
        \end{pmatrix}.
    \end{equation*}

    Step 2, dividing the second row by $-(k_2 + k_3 + [S] k_1)$ to have 1 on the diagonal:
    \begin{equation*}
        \begin{pmatrix}
            1 & 1 & 1 \\
            0 & 1 & \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1}\\
            0 & k_3 -[P] k_6 & -(k_4+k_5) - [P] k_6\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            1\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            - [P] k_6\\
        \end{pmatrix}.
    \end{equation*}

    Step 3, subtracting the second row from the 1st, and again from the 3rd (after multiplying by $k_3 - [P]k_6$):
    \begin{equation*}
        \begin{pmatrix}
            1 & 0 & 1 - \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1} \\
            0 & 1 & \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1}\\
            0 & 0 & -(k_4+k_5) - [P] k_6 - \frac{([S] k_1 - k_4)(k_3 -[P] k_6)}{k_2 + k_3 + [S] k_1}\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            1 - \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            - [P] k_6 - \frac{[S] k_1 (k_3 -[P] k_6)}{k_2 + k_3 + [S] k_1}
        \end{pmatrix}.
    \end{equation*}
    which after simplifying becomes:
    \begin{equation*}
    \begin{pmatrix}
            1 & 0 & \frac{k_2+k_3+k_4}{k_2 + k_3 + [S] k_1} \\
            0 & 1 & \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1}\\
            0 & 0 & - \frac{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}{k_2 + k_3 + [S] k_1}\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            \frac{k_2+k_3}{k_2 + k_3 + [S] k_1}\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            -\frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3 }{k_2 + k_3 + [S] k_1}
        \end{pmatrix}.
    \end{equation*}
    and we normalize the last row to have 1 on the diagonal:
    \begin{equation*}
    \begin{pmatrix}
            1 & 0 & \frac{k_2+k_3+k_4}{k_2 + k_3 + [S] k_1} \\
            0 & 1 & \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1}\\
            0 & 0 & 1\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            \frac{k_2+k_3}{k_2 + k_3 + [S] k_1}\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1}\\
            \frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}
        \end{pmatrix}.
    \end{equation*}

    Step 4, we eliminate the off-diagonal values of the third column using the 3rd row:
    \begin{eqnarray*}
        \begin{pmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0\\
            0 & 0 & 1\\
        \end{pmatrix}
        \begin{pmatrix}
            [E]\\
            [ES]\\
            [EP]
        \end{pmatrix}
        =
        [E_\text{tot}]
        \begin{pmatrix}
            \frac{k_2+k_3}{k_2 + k_3 + [S] k_1} - \frac{k_2+k_3+k_4}{k_2 + k_3 + [S] k_1} \cdot \frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
            \frac{[S] k_1}{k_2 + k_3 + [S] k_1} - \frac{[S] k_1 - k_4}{k_2 + k_3 + [S] k_1} \cdot \frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
            \frac{[P] k_6 k_2 + [P] k_6 k_3 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}
        \end{pmatrix}
    \end{eqnarray*}

    Simplifying the expressions on the right-hand side is a lengthy process (which we do not show here) and in the end we get:
    \begin{eqnarray*}
    \begin{pmatrix}
        1 & 0 & 0 \\
        0 & 1 & 0\\
        0 & 0 & 1\\
    \end{pmatrix}
    \begin{pmatrix}
        [E]\\
        [ES]\\
        [EP]
    \end{pmatrix}
    =
    [E_\text{tot}]
    \begin{pmatrix}
        \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
        \frac{[P] k_4 k_6 + [S] k_1 k_4 + [S] k_1 k_5}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
        \frac{[P] k_2 k_6 + [P] k_3 k_6 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}\\
    \end{pmatrix}
\end{eqnarray*}

    Therefore,
    \begin{align}
        [E] &= [E_\text{tot}] \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5} \\
        [ES] &= [E_\text{tot}] \frac{[P] k_4 k_6 + [S] k_1 k_4 + [S] k_1 k_5}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5} \\
        [EP] &= [E_\text{tot}] \frac{[P] k_2 k_6 + [P] k_3 k_6 + [S] k_1 k_3}{[S]k_1(k_3 + k_4 + k_5) + [P]k_6(k_2+k_3+k_4) + k_2 k_4 + k_2 k_5 + k_3 k_5}
    \end{align}

\end{document}
Cost of metabolic pathways
==========================

To open the example Jupyter notebook, first clone this repository and set up a
virtual environment:
```bash
git clone https://gitlab.com/principlescellphysiology/book-economic-principles-in-cell-biology.git
python -m venv venv
source venv/bin/activate
pip install equilibrator-pathway jupyter
```

Then launch the Jupyter notebook:
```bash
cd summer-school-2022/lpi-slides/lectures/PAT/exercises
jupyter notebook
```

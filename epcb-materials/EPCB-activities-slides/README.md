Announcement slides
===================

Please feel free to use these slides to announce the Forum and our book project in your home institution or at workshops and conferences.

Feel free to modify the slides and send us your updated, nicer versions! (by email to Wolf or Elad).
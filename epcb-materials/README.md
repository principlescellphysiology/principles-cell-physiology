EPCB - Documents / other materials for internal usage
=====================================================

* `advertisement-slides`: Slides for announcing the Forum and book project
* `book-poster`: Book advertisement poster
* `epcp-melody`: Jingle
* `qr-codes`: QR codes for different purposes
